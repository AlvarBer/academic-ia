package app.solvers;

import app.main.Problema;
import app.main.Solution;

public interface Solver {
    Solution solve(Problema p) throws SolutionNotFoundException;
}
