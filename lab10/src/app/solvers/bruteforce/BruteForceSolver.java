package app.solvers.bruteforce;

import app.main.Problema;
import app.main.Circulo;
import app.main.Solution;
import app.solvers.Solver;

public class BruteForceSolver implements Solver {

	@Override
	public Solution solve(Problema p) {

		Circulo c = new Circulo(0, 0, 1);
		Circulo max = new Circulo(0, 0, 1);

        boolean found = false;
        for (int r = p.DIMENSION / 2; r > 0 && !found; r--) {
            for (int x = r; x <= p.DIMENSION && !found; x++) {
                for (int y = r; y <= p.DIMENSION && !found; y++) {
                    c.transform(x, y, r);
                    if (p.esSolucion(c)) {
                        max = new Circulo(x, y, r);
                        found = true;
                    }
                }
            }
        }

		return new Solution("BruteForce", max);
	}
}
