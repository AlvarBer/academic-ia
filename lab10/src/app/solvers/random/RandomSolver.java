package app.solvers.random;

import app.main.Circulo;
import app.main.Problema;
import app.main.Solution;
import app.main.Util;
import app.solvers.Solver;

import java.util.PriorityQueue;

public class RandomSolver implements Solver {

	private static final int NUM_CANDIDATES = 100000;

	@Override
	public Solution solve(Problema p) {
		return bestSolution(p, generateCandidates(p));
	}

	private PriorityQueue<Circulo> generateCandidates(Problema p) {
		PriorityQueue<Circulo> solutions = new PriorityQueue<>();
		int dim = Util.randomInt(1, Problema.DIMENSION);

		Circulo c = Circulo.random(dim);

		while (solutions.size() < NUM_CANDIDATES) {
			if (p.esSolucion(c))
				solutions.add(Circulo.random(dim));

			c = Circulo.random(dim);
		}

		return solutions;
	}

	public Solution bestSolution(Problema p, PriorityQueue<Circulo> solutions) {
		Circulo s = solutions.remove();
		while (!p.esSolucion(s)) {
			s = solutions.remove();
		}

		return new Solution("Random", s);
	}
}
