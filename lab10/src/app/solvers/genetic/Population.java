package app.solvers.genetic;


import app.main.Circulo;
import app.main.Individuo;
import app.main.Problema;
import app.main.Util;
import app.solvers.SolutionNotFoundException;
import lib.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Population {

    private int generation = 0;
    private PriorityQueue<Individuo> individuals;
    private Problema p;
    private float[] rouletteFrequencies;

    private Logger log;

    public Population(int size, Problema p) {
        individuals = new PriorityQueue<>();
        generateIndividuals(size);
        generation = 0;
        this.p = p;

        rouletteFrequencies = new float[individuals.size()];

        log = new Logger("Genetic Solver");
    }

    private void generateIndividuals(int size) {
        for (int i = 0; i < size; i++) {
            individuals.add(new Individuo(Circulo.random(Problema.DIMENSION)));
        }
    }

    public boolean stop() {
        return generation == GeneticSolver.MAX_GENERATIONS;
    }

    public void procreate() {
        refreshFitness();
        log.logSubtitle("Generation " + generation);

        survival();
        generation++;

        log.log("Current", toString());
    }

    private void survival() {
        updateFrequencies();

        PriorityQueue<Individuo> newGeneration = initializeWithElite();

        while (newGeneration.size() < individuals.size()) {

            Individuo[] selectedIndividuals = rouletteExtract();

            Pair pair = new Pair(selectedIndividuals[0], selectedIndividuals[1]);
            log.log("Selected Pair", pair);

            boolean cross = pair.cross();
            log.log("After Crossing: " + cross, pair);

            boolean mutate = pair.mutate();
            log.log("After Mutating: " + mutate, pair);

            newGeneration.addAll(pair.toList());
        }

        individuals = new PriorityQueue<>(newGeneration);
    }

    private PriorityQueue<Individuo> initializeWithElite() {
        PriorityQueue<Individuo> result = new PriorityQueue<>();
        if (GeneticSolver.PRESERVE_ELITE) {
            int i = 0;
            for (Individuo ind : individuals) {
                result.add(ind);
                i++;
                if (i >= GeneticSolver.ELITE_SIZE)
                    break;
            }
        }
        return result;
    }

    public Individuo best() throws SolutionNotFoundException {
        Individuo s = individuals.remove();
        while (!p.esSolucion(s.toCirculo())) {
            s = individuals.poll();

            if (s == null)
                throw new SolutionNotFoundException("Genetic Solver", "Genetic Solver couldn't find a solution");
        }
        return s;
    }

    private void refreshFitness() {
        for (Individuo i : individuals) {
            i.setFitness(fitness(i));
        }
    }

    private Individuo[] rouletteExtract() {

        Individuo[] result = new Individuo[2];

        int firstIndex = Util.fromArbitraryInterval(Util.random(1), rouletteFrequencies);
        int secondIndex = Util.fromArbitraryInterval(Util.random(1), rouletteFrequencies);

        // Super dirty trick to circumvent the PrioQueue restrictions
        // It works because the rouletteFrequencies is sorted exactly like
        // The PrioQueue
        int i = 0;
        for (Individuo ind : individuals) {
            if (i == firstIndex) {
                result[0] = ind;
            }
            if (i == secondIndex) {
                result[1] = ind;
            }
            i++;
        }
        return result;
    }

    private int fitness(Individuo i) {
        if (!p.esSolucion(i.toCirculo()))
            return 0;

        if (i.toCirculo().getRadio() > Problema.DIMENSION / 2)
            return 0;

        return i.toCirculo().getRadio();
    }

    private void updateFrequencies() {
        int totalFitness = calculateTotalFitness();
        int ndx = 0;
        for (Individuo i : individuals) {
            rouletteFrequencies[ndx] = i.getFitness() / totalFitness;
            ndx++;
        }

    }

    private int calculateTotalFitness() {
        int total = 0;
        for (Individuo i : individuals) {
            total += i.getFitness();
        }
        return total;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Individuo i : individuals) {
            sb.append("- " + i + "\n");
        }
        sb.append("\n");

        return sb.toString();
    }

    public void writeHistory() {
        log.dump();
    }
}

