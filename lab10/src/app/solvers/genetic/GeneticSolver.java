package app.solvers.genetic;

import app.main.Circulo;
import app.main.Problema;
import app.main.Solution;
import app.solvers.SolutionNotFoundException;
import app.solvers.Solver;

public class GeneticSolver implements Solver {

    protected static final int POPULATION_SIZE = 10;
    protected static final int MAX_GENERATIONS = 100000;

    protected static final int NUM_GENES = 3;
    protected static final float CROSSOVER_RATE = 0.7f;
    protected static final float MUTATION_RATE = 0.3f;
    protected static final float GENE_MUTATION_TRESHOLD = 0.01f;

    protected static final boolean PRESERVE_ELITE = true;
    protected static final int ELITE_SIZE = 2;

    @Override
	public Solution solve(Problema p) throws SolutionNotFoundException {

        Population population = new Population(POPULATION_SIZE, p);

        while (!population.stop())
           population.procreate();

        population.writeHistory();

        return new Solution("Genetic", population.best().toCirculo());
	}

}
