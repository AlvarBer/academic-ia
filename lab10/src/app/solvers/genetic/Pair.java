package app.solvers.genetic;

import app.main.Individuo;
import app.main.Util;

import java.util.ArrayList;
import java.util.List;

public class Pair {

    Individuo[] pair;

    public Pair(Individuo x, Individuo y) {
        pair = new Individuo[2];
        pair[0] = x;
        pair[1] = y;
    }

    public boolean cross() {
        if (!(Util.random(1) <= GeneticSolver.CROSSOVER_RATE))
            return false;

        // Cross
        int crossPoint = 10 * Util.fromEquidistantInterval(Util.random(1), GeneticSolver.NUM_GENES - 1);
        swapGenes(crossPoint);

        return true;
    }

    private void swapGenes(int cp) {
        String tmp = new String(pair[0].getEndOfCromosom(cp));
        pair[0] = new Individuo(pair[0].getStartOfCromosom(cp).concat(pair[1].getEndOfCromosom(cp)), 0);
        pair[1] = new Individuo(pair[1].getStartOfCromosom(cp).concat(tmp), 0);
    }


    public boolean mutate() {
        if (!(Util.random(1) <= GeneticSolver.MUTATION_RATE))
            return false;

        // Mutate
        for (int ind = 0; ind < pair.length; ind++) {
            for (int i = 0; i < pair[ind].getCromosoma().length(); i++) {
                if (Util.random(1) <= GeneticSolver.GENE_MUTATION_TRESHOLD) {
                    char c = Util.inverseChar(pair[ind].getCromosoma().charAt(i));
                    String n = pair[ind].getStartOfCromosom(i) + c + pair[ind].getEndOfCromosom(i + 1);
                    pair[ind] = new Individuo(n, 0);
                }
            }
        }

        return true;
    }

    public List<Individuo> toList (){
        List<Individuo> res = new ArrayList<>();
        for(Individuo i : pair) {
            res.add(i);
        }

        return res;
    }

    @Override
    public String toString() {
        return "- " + pair[0] + "\n" +
                "- " + pair[1] + "\n\n";
    }
}
