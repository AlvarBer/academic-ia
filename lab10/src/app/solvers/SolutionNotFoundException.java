package app.solvers;

public class SolutionNotFoundException extends RuntimeException {

    private String solver;

    public SolutionNotFoundException(String msg) {
        super(msg);
    }

    public SolutionNotFoundException(String solver, String msg) {
        super(msg);
        this.solver = solver;
    }

    public String getSolver() {
        return solver;
    }
}
