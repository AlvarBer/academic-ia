package app.main;


/**
 * Representa un círculo a partir de su centro y su radio.
 * 
 * @author Antonio Sánchez
 */
public class Circulo implements Comparable<Circulo>{
	private int x, y;  // centro del círculo
	private int radio;

	/**
	 * Devuelve un círculo aleatorio con 0 <= x,y < dimensiones
	 * y 0 <= r < dimensiones / 2.
	 */
	public static Circulo random(int dimensiones) {
		int x = Util.randomInt(0, dimensiones);
		int y = Util.randomInt(0, dimensiones);
		int r = Util.randomInt(1, dimensiones / 2);
		return new Circulo(x, y, r);
	}
	
	/**
	 * Constructor a partir del centro (x,y) y el radio.
	 */
	public Circulo(int x, int y, int radio) {
		this.x = x;
		this.y = y;
		this.radio = radio;
	}

	public Circulo(String circle) {
		String[] tokens = circle.split(",");
		this.x = Integer.parseInt(tokens[0].substring(tokens[0].indexOf("=")+ 1, tokens[0].length()));
		this.y = Integer.parseInt(tokens[1].substring(tokens[1].indexOf("=")+ 1, tokens[1].length()));
		this.radio = Integer.parseInt(tokens[2].substring(tokens[2].indexOf("=")+ 1, tokens[2].indexOf("]")));
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getRadio() {
		return radio;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setRadio(int radio) {
		this.radio = radio;
	}
	
	public void transform(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radio = radius;
	}

	/**
	 * Calcula si hay intersección entre este círculo y c. Si sólo se tocan en 
	 * un punto (son tangentes) decimos que no hay intersección.
	 */
	public boolean interseccion(Circulo c) {
		return Util.distEuclidea(x, y, c.x, c.y) < radio + c.radio;
	}
	
	/**
	 *  Calcula si este círculo está dentro del cuadrado apoyado en el (0,0) 
	 *  con longitud de lado dimensiones.
	 */
	public boolean dentroDeCuadrado(int dimensiones) {

		return (radio <= x) && (radio <= y) && (radio < dimensiones - x) && (radio < dimensiones - y);
	}

	@Override
	public String toString() {
		return "Circulo [x=" + x + ", y=" + y + ", r=" + radio + "]";
	}

	@Override
	public int compareTo(Circulo other) {
		return other.getRadio() - radio;
	}
}
