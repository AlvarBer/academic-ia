package app.main;

/**
 * Utilidades.
 * 
 * @author Antonio Sánchez
 */
public class Util {

	/**
	 *  Valor entero aleatorio en [min, max).
	 */
	public static int randomInt(int min, int max) {
		return (int) Math.floor(Math.random() * (max - min + 1) + min);  
	}
	
	/**
	 *  Valor float aleatorio en [0, 1)
	 */
	public static float random() {
		return (float) Math.random();
	}
	
	/**
	 *  Valor float aleatorio en [0, max)
	 */
	public static float random(float max) {
		return (float) Math.random() * max;
	}
	
	/**
	 *  Distancia euclidea entre los puntos (x1, y1) y (x2, y2)
	 */
	public static float distEuclidea(int x1, int y1, int x2, int y2) {
		return (float) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	/**
	 * @return interval in which p is located
	 */
	public static int fromEquidistantInterval(float p, int pieces) {
		float step = 1f/pieces;
		float interval = step;
		int res = 1;
		while (interval < p) {
			res++;
			interval += step;
		}

		return res;
	}

	/**
	 * @param lengths array of the lengths of the segments - lengths[i] = length of the i-th segment
	 *
	 * @return index i of the first segment that fulfills: point <= sum(lenghts[0]...lengths[i])
	 */
	public static int fromArbitraryInterval(float point, float[] lengths) {
		int res = 0;
		float total = 0f;

		while (point > total && res < lengths.length) {
            total += lengths[res];
            res++;
		}

		return res - 1;
	}

	/**
	 * @return '0' if c == '1', '1' otherwise
 	 */
	public static char inverseChar(char c) {
		if (c == '1')
			return '0';

		return '1';
	}
}
