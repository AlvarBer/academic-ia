package app.main;

import app.generator.CaseGenerator;
import app.solvers.SolutionNotFoundException;
import app.solvers.Solver;
import app.solvers.bruteforce.BruteForceSolver;
import app.solvers.genetic.GeneticSolver;
import app.solvers.random.RandomSolver;
import app.views.CirclesWindow;
import lib.parser.ConsoleArgumentParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static javax.swing.JOptionPane.showMessageDialog;

public class Main {

    private static final String[] ARGUMENTS = {"-g", "-s", "-f", "-d", "-t"};

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println(System.getProperty("user.dir"));
        ConsoleArgumentParser parser = new ConsoleArgumentParser(ARGUMENTS);
        Map<String, String> arguments = parser.parseArguments(args);

        Problema problem = null;
        List<Solution> solutions = null;

        if (arguments.containsKey("-d")) {
            problem = loadProblem(arguments.get("-d") + "/problem.txt");
            solutions = loadSolutions(arguments.get("-d") + "/solutions.txt");
            CirclesWindow window = new CirclesWindow(problem, solutions);
            return;
        }

        if (arguments.containsKey("-g")) {
            CaseGenerator.generate("./cases/case001.txt");
        }

        if (arguments.containsKey("-f")) {
            problem = loadProblem(arguments.get("-f"));
        } else {
            problem = loadProblem("./cases/case001.txt");
        }

        solutions = new ArrayList<>();
        long[] times = null;
        long startTime;
        // -f="path" -s=BRG -t
        if (arguments.containsKey("-s")) {
            String solvers = arguments.get("-s");
            times = new long[solvers.length()];
            try {
                for (int i = 0; i < solvers.length(); i++) {

                    startTime = System.currentTimeMillis();

                    Solver solver = instantiateSolver(solvers.charAt(i));
                    solutions.add(solver.solve(problem));

                    times[i] = System.currentTimeMillis() - startTime;
                }
            } catch (SolutionNotFoundException e) {
                displayError(e.getMessage());
            }
        }

        if (arguments.containsKey("-t")) {
            for (int i = 0; i < solutions.size(); i++) {
                System.out.println("- " + solutions.get(i) + ": T=" + times[i]);
            }
        }

        CirclesWindow board = new CirclesWindow(problem, solutions);
    }

    private static List<Solution> loadSolutions(String path) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(path));
        List<Solution> solutions = new ArrayList<>();

        int numSols = sc.nextInt();sc.nextLine();
        for (int i=0; i<numSols; i++) {
            solutions.add(new Solution(sc.nextLine()));
        }
        sc.close();

        return solutions;
    }


    private static Solver instantiateSolver(char c) {
        Solver s;
        switch (c) {
            case 'B':
                s = new BruteForceSolver();
                break;
            case 'R':
                s = new RandomSolver();
                break;
            case 'G':
                s = new GeneticSolver();
                break;
            default:
                s = new BruteForceSolver();
        }
        return s;
    }

    private static Problema loadProblem(String path) {
        Problema p = null;

        try {
            p = new Problema(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return p;
    }

    private static Circulo loadSolution(String path) {
        Circulo c;

        c = new Circulo(path);

        return c;
    }

    private static void displayError(String msg) {
        showMessageDialog(null, msg);
    }
}
