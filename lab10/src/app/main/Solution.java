package app.main;

public class Solution {

    private Circulo circle;
    private String solver;

    public Solution(String solver, Circulo circle) {
        this.solver = solver;
        this.circle = circle;
    }

    public Solution(String solution) {
        this.solver = solution.substring(0, solution.indexOf(":"));
        this.circle = new Circulo(solution.substring(
                                        solution.indexOf(":") + 1, solution.length()));
    }

    public Circulo getCircle() {
        return circle;
    }

    public String getSolver() {
        return solver;
    }

    public String toString() {
        return solver + ": " + circle.toString();
    }
}
