package app.views;

import app.main.Circulo;
import app.main.Problema;
import app.main.Solution;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class CirclesBoard extends JPanel {
    private static final Color[] COLORS = { Color.BLUE,
                                            Color.RED,
                                            Color.YELLOW,
                                            Color.WHITE};

    private Problema p;
    private List<Solution> solutions;

    public CirclesBoard(Problema p, List<Solution> sol) {
        this.p = p;
        this.solutions = sol;

        initProperties();
    }

    private void initProperties() {
        this.setPreferredSize(new Dimension(Problema.DIMENSION, Problema.DIMENSION));
    }

    public void paint(Graphics g) {
        super.paintComponent(g);

        drawBackground(g);
        drawProblem(g);
        drawSolutions(g);

    }

    private void drawBackground(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, Problema.DIMENSION, Problema.DIMENSION);
    }

    private void drawProblem(Graphics g) {
        g.setColor(Color.BLACK);
        for (Circulo c : p.getCirculos()) {
            drawCircle(g, c, Color.BLACK);
            //g.fillOval(c.getX() - c.getRadio(), c.getY() - c.getRadio(), c.getRadio() * 2, c.getRadio() * 2);
        }
    }

    private void drawSolutions(Graphics g) {
        for(int i = 0; i < solutions.size(); i++) {
            Solution solution = solutions.get(i);
            drawCircle(g, solution.getCircle(), COLORS[i]);
            //drawText(g, solution.getSolver(), solution.getCircle(), Color.WHITE);
        }
    }

    private void drawCircle(Graphics g, Circulo s, Color c) {
        g.setColor(c);
        g.fillOval(s.getX() - s.getRadio(), s.getY() - s.getRadio(),
                    s.getRadio() * 2, s.getRadio() * 2);
    }

    private void drawText(Graphics g, String text, Circulo circle, Color color) {
        g.setColor(color);
        g.drawChars(text.toCharArray(), 0, text.length(),
                        circle.getX(),
                        circle.getY());
    }
}
