package app.views;

import app.generator.CaseGenerator;
import app.main.Circulo;
import app.main.Problema;
import app.main.Solution;
import app.solvers.SolutionNotFoundException;
import app.solvers.bruteforce.BruteForceSolver;
import app.solvers.genetic.GeneticSolver;
import app.solvers.random.RandomSolver;
import app.solvers.Solver;
import lib.parser.ConsoleArgumentParser;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
//import java.util.stream.Collectors;

import static javax.swing.JOptionPane.showMessageDialog;

public class CirclesWindow extends JFrame {

    public CirclesWindow(Problema p, List<Solution> sol) {
        super("Solutions");

        initComponents(p, sol);
        initProperties();
    }

    private void initComponents(Problema p, List<Solution> sol) {

        CirclesBoard c = new CirclesBoard(p, sol);
        this.add(c);
        c.paintComponents(null);

    }

    private void initProperties() {
        this.pack();
        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);
    }

}

