package app.generator;

import app.main.Circulo;
import app.main.Problema;
import app.main.Solution;
import app.solvers.SolutionNotFoundException;
import app.solvers.Solver;
import app.solvers.bruteforce.BruteForceSolver;
import app.solvers.genetic.GeneticSolver;
import app.solvers.random.RandomSolver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class SolutionGenerator {
    private static final int NUM_CASES = 3;
    private static final String PATH = "./lab10/cases/";

    public static void main(String[] args) throws FileNotFoundException {
        List<Solver> solvers = initSolvers();
        List<Solution> solutions = new ArrayList<>();

        for (int i = 0; i < NUM_CASES; i++) {
            System.out.print("Solving case"+i+"...");

            new File(PATH+i).mkdirs();

            String problemFname = PATH+i+"/problem.txt";
            String solutionsFname = PATH+i+"/solutions.txt";

            CaseGenerator.generate(problemFname);

            Problema p = new Problema(problemFname);

            for (Solver s : solvers) {
                try {
                    solutions.add(s.solve(p));
                } catch (SolutionNotFoundException e) {
                    solutions.add(new Solution(e.getSolver(), new Circulo(0, 0, 0)) );
                }
            }

            printSolutions(solutionsFname, solutions);

            System.out.println("Solved");
        }
    }

    private static void printSolutions(String solutionsFname, List<Solution> solutions) {
        PrintWriter writer;

        try {
            writer = new PrintWriter(solutionsFname);

            writer.write("3\n");
            writer.write(solutions.get(0) + "\n");
            writer.write(solutions.get(1) + "\n");
            writer.write(solutions.get(2) + "\n");

            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static List<Solver> initSolvers() {
        List<Solver> solvers = new ArrayList<>();

        solvers.add(new BruteForceSolver());
        solvers.add(new RandomSolver());
        solvers.add(new GeneticSolver());

        return solvers;
    }
}
