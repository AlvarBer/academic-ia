/* 
 * @author: Borja Lorente Escobar
 * @author: Álvaro Bermejo García
 */

package app.generator;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import app.main.Circulo;
import app.main.Problema;
import app.main.Util;

public class CaseGenerator {
	public static int MAX_CIRCLES = 20;

	public static void generate(String filename) {
		int n = Util.randomInt(0, MAX_CIRCLES);

		int maxCirc = Problema.DIMENSION / (n + 1);

		try {
			PrintWriter writer = new PrintWriter(filename);

			writer.print(Integer.toString(n));
			writer.print(" ");

			for (int i = 0; i < n; i++) {
				Circulo c = Circulo.random(maxCirc);
				maxCirc += (Problema.DIMENSION / n);

				writer.print(Integer.toString(c.getX()) + " ");
				writer.print(Integer.toString(c.getY()) + " ");
				writer.print(Integer.toString(c.getRadio()) + " ");
			}

			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
