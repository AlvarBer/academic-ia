# Genetic Solver Settings

```java
    protected static final int POPULATION_SIZE = 10;
    protected static final int MAX_GENERATIONS = 100000;

    protected static final int NUM_GENES = 3;
    protected static final float CROSSOVER_RATE = 0.7f;
    protected static final float MUTATION_RATE = 0.3f;
    protected static final float GENE_MUTATION_TRESHOLD = 0.01f;

    protected static final boolean PRESERVE_ELITE = true;
    protected static final int ELITE_SIZE = 2;
```

# Random Solver Settings

```java
private static final int NUM_CANDIDATES = 100000;
```

# Color code

- BLUE: Brute Force
- RED: Random
- YELLOW: Genetic

# Case0

- BruteForce: Circulo [x=511, y=511, r=511]: T=21
- Random: Circulo [x=495, y=459, r=448]: T=44
- Genetic: Circulo [x=585, y=595, r=420]: T=4135

# Case1

- BruteForce: Circulo [x=87, y=87, r=87]: T=3413
- Random: Circulo [x=80, y=919, r=77]: T=106
- Genetic: Couldn't find a solution

# Case2

- BruteForce: Circulo [x=215, y=808, r=215]: T=1732
- Random: Circulo [x=52, y=50, r=50]: T=72
- Genetic: Circulo [x=815, y=230, r=194]: T=3757

# Case3

- BruteForce: Circulo [x=502, y=515, r=101]: T=3566
- Random: Circulo [x=476, y=468, r=49]: T=1008
- Genetic: Circulo [x=511, y=509, r=62]: T=3359

# Case4

- BruteForce: Circulo [x=502, y=515, r=337]: T=477
- Random: Circulo [x=68, y=79, r=55]: T=62
- Genetic: Circulo [x=496, y=566, r=261]: T=3521

# Case5

- BruteForce: Circulo [x=149, y=150, r=50]: T=82220
- Random: Circulo [x=60, y=62, r=35]: T=253
- Genetic: Circulo [x=247, y=518, r=26]: T=5834

# Case6

- BruteForce: Circulo [x=820, y=263, r=202]: T=3797
- Random: Circulo [x=549, y=236, r=124]: T=293
- Genetic: Couldn't find a solution

# Case7

- BruteForce: Circulo [x=180, y=842, r=180]: T=4784
- Random: Circulo [x=46, y=113, r=42]: T=1231
- Genetic: Circulo [x=191, y=852, r=159]: T=3376

# Case8

- BruteForce: Circulo [x=808, y=810, r=213]: T=3841
- Random: Circulo [x=780, y=816, r=205]: T=234
- Genetic: Couldn't find a solution

# Case9

Se queda clavado en random solver, pero creo que podemos ignorarlo
