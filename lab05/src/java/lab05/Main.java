package lab05;

import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

import jess.*;

public class Main {
	private static final int MAX = 10;

	public static void main(String[] args) throws JessException {
		Rete engine = new Rete();
		String filePath = System.getProperty("user.dir");
		if(!filePath.contains("lab05"))
			filePath += "/lab05";
		System.out.println("Loading Rules");
		try {
			Value v = engine.batch(filePath + "/src/jess/gaminator.clp");
			System.out.println("Did it read it right? " + v);
		} catch (JessException je0) {
			System.out.println("Read failure of gaminator.clp");
			je0.printStackTrace();
		}
		engine.addDeffacts(inputUser(engine));
        //engine.addDeffacts(hardcodedUser(engine));
		engine.reset();
		engine.setFocus("myMAIN");
		engine.run();
		//listAllFacts(engine);
		extractFacts(engine);
		engine.halt(); // Stop rule engine
	}

	private static Deffacts inputUser(Rete engine) throws JessException {
		Scanner in = new Scanner(System.in);
		int waitTolerate;
		String inputStr;
		ValueVector themes = new ValueVector();
		Deffacts deffacts = new Deffacts("InputtedUser", null, engine);
		Fact f = new Fact("USER::user", engine);
		System.out.println("How old are you?");
		f.setSlotValue("age", new Value(in.nextInt(), RU.INTEGER));
        System.out.println("How long have you been playing? (YEARS)");
        f.setSlotValue("howLongPlaying", new Value(in.nextInt(), RU.INTEGER));
        System.out.println("How much are you willing to spend? (EUROS)");
        f.setSlotValue("budget", new Value(in.nextInt(), RU.INTEGER));
		System.out.println("How many friends you want to play with?");
		f.setSlotValue("numFriends", new Value(in.nextInt(), RU.INTEGER));
		do {
			System.out.println("In a scale from 1 to 10, how much do you tolerate waiting for your turn?");
			waitTolerate = in.nextInt();
		} while (waitTolerate > 10 || waitTolerate < 1);
		f.setSlotValue("likesToWait", new Value(waitTolerate, RU.INTEGER));
		System.out.println("How much time do you have to play (In minutes)?");
		f.setSlotValue("timeToPlay", new Value(in.nextInt(), RU.INTEGER));
		do {
			System.out.println("Are you a competitive person?(TRUE/FALSE)");
			inputStr = in.next();
		} while (!inputStr.equals("TRUE") && !inputStr.equals("FALSE"));
		f.setSlotValue("isCompetitive", new Value(inputStr, RU.SYMBOL));
		do {
			System.out.println("Do you like surprises?(TRUE/FALSE)");
			inputStr = in.next();
		} while (!inputStr.equals("TRUE") && !inputStr.equals("FALSE"));
		f.setSlotValue("likesSurprises", new Value(inputStr, RU.SYMBOL));
		do {
			System.out.println("Wanna put your thinking hat on?(TRUE/FALSE)");
			inputStr = in.next();
		} while (!inputStr.equals("TRUE") && !inputStr.equals("FALSE"));
		f.setSlotValue("wantsToThink", new Value(inputStr, RU.SYMBOL));
		do {
			System.out.println("Wanna get schwifty?(TRUE/FALSE)");
            inputStr = in.next();
		} while (!inputStr.equals("TRUE") && !inputStr.equals("FALSE"));
		f.setSlotValue("schwifty", new Value(inputStr, RU.SYMBOL));
		do {
		    System.out.println("Are you going to play many times?(TRUE/FALSE)");
            inputStr = in.next();
		} while (!inputStr.equals("TRUE") && !inputStr.equals("FALSE"));
		f.setSlotValue("wantsDurableComponents", new Value(inputStr, RU.SYMBOL));
		System.out.println("Want to explore the jungle(y/n)?");
		if (in.next().equals("y"))
			themes.add(new Value("Jungle", RU.SYMBOL));
		System.out.println("Are you afraid of spooky skeletons(y/n)?");
		if (in.next().equals("y"))
			themes.add(new Value("Horror", RU.SYMBOL));
		System.out.println("Always wanted to sail the seven seas(y/n)?");
		if (in.next().equals("y"))
			themes.add(new Value("Pirates", RU.SYMBOL));
		System.out.println("Want some Fantasy to evade your reality(y/n)?");
		if (in.next().equals("y"))
			themes.add(new Value("Fantasy", RU.SYMBOL));
		System.out.println("Do you like planes(y/n)?");
		if (in.next().equals("y"))
			themes.add(new Value("Planes", RU.SYMBOL));
		f.setSlotValue("likedThemes", new Value(themes, RU.LIST));

		deffacts.addFact(f);

		return deffacts;
	}

	private static Deffacts hardcodedUser(Rete engine) throws JessException {
		Deffacts deffacts = new Deffacts("InputtedUser", null, engine);
		Fact f = new Fact("USER::user", engine);
		f.setSlotValue("age", new Value(3, RU.INTEGER));
		f.setSlotValue("numFriends", new Value(4, RU.INTEGER));
		f.setSlotValue("likesToWait", new Value(3.0, RU.INTEGER));
		f.setSlotValue("timeToPlay", new Value(200, RU.INTEGER));
		f.setSlotValue("isCompetitive", new Value("true", RU.SYMBOL));
		deffacts.addFact(f);

		return deffacts;
	}

	public static void listAllFacts(Rete engine) { // Get & Print facts
		Iterator<Fact> i = engine.listFacts();
		System.out.println("Fact List:");
		while (i.hasNext())
			System.out.println(i.next());
	}

	public static void extractFacts(Rete engine) { // Get only a subset of facts
		Iterator<Fact> i = engine.listFacts();
		Value ed;
		System.out.println("The games you should play are:");
		int numberGames = 0;
		while (i.hasNext() && numberGames < MAX) {
			Fact fact = i.next();
			if (fact.getName().equals("RECOMENDATIONS::recommendedGame")) {
				++numberGames;
				try {
					ed = fact.getSlotValue("name");
					System.out.println(ed);
				} catch (JessException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
