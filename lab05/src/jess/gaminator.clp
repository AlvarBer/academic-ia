;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BASE DECLARATIONS OF TEMPLATES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmodule USER)
(deftemplate USER::user
	(slot age (type NUMBER) (default 20))
	(slot howLongPlaying (type NUMBER) (default 4))
	(slot numFriends (type NUMBER) (default 3))
	(slot budget (type NUMBER) (default 1000))
	(slot mentalEnergy (type NUMBER) (default 10))
	(slot timeToPlay (type NUMBER) (default 200))
	(slot isCompetitive (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot likesToWait (type NUMBER) (default 1.0) (allowed-values 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0))
	(slot likesSurprises (type SYMBOL) (default TRUE) (allowed-values TRUE FALSE))
	(slot wantsToThink (type SYMBOL) (default TRUE) (allowed-values TRUE FALSE))
	(slot schwifty (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
    (slot wantsDurableComponents (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
    (multislot likedThemes (type SYMBOL) (default NONE Jungle Horror Pirates Fantasy Planes)
                                         (allowed-values NONE Jungle Horror Pirates Fantasy Planes))
	)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RECOMMENDATIONS IS THE INFERENCE MODULE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmodule RECOMMENDATIONS)
(deftemplate RECOMMENDATIONS::recommendedGame
	(slot name (type SYMBOL))
	)

(deffunction RECOMMENDATIONS::maxTurnLength (?likesToWait ?timeToPlay ?EPA ?avgTurnLength)
	(return (* (/ ?timeToPlay (/ ?EPA ?avgTurnLength)) (/ ?likesToWait 2)))
	)

(deffunction RECOMMENDATIONS::minDurability (?durable ?budget ?age)
    (if (eq ?durable TRUE) then
        (return (+ (+ (* (/ 50 ?budget) 0.25) (* (/ 5 ?age) 0.25)) 0.25))
    else
        (return (+ (* (/ 50 ?budget) 0.25) (* (/ 5 ?age) 0.25)))
    )
)

(deffunction RECOMMENDATIONS::maxDelta (?likesSurprises ?likesThink)
    (if (eq ?likesSurprises TRUE) then
        (if (eq ?likesThink TRUE) then
            (return 0.75)
        else
            (return 1)
        )
    else
        (return 0.5)
    )
)

(deffunction RECOMMENDATIONS::maxComplexity (?likesThink ?age ?mentalEnergy)
    (if (eq ?likesThink TRUE) then
        (return (+ 0.25 (+ (/ ?age 15) ?mentalEnergy)) )
    else
        (return (- 0.25 (+ (/ ?age 15) ?mentalEnergy)) )
    )
)

(deffunction RECOMMENDATIONS::minReplayability (?budget)
    (return (/ 10 ?budget) )
)

(deffunction RECOMMENDATIONS::canBuyExpansions (?price ?budget)
    (if (>= (- ?budget ?price) (/ ?price 2)) then
        (return TRUE)
    else
        (return FALSE)
    )
)

(deffunction RECOMMENDATIONS::needsToBeReusable (?bu ?age)
    (if (<= ?age 15) then
        (return TRUE)
    else
        (if (<= ?bu 10) then
           (return TRUE)
        else
           (return FALSE)
        )
    )
)

(deffunction RECOMMENDATIONS::minInteractivity (?nplayers ?maxTurnLength)
    (return (+ (* 0.5 (/ ?nplayers 10)) (* 0.5 (/ ?maxTurnLength 10))))
)

(defmodule GAMEBASE)
(deftemplate gameBase
    (slot name (type SYMBOL) (default "Game"))
    (slot price (type NUMBER) (default 0))
    (slot EPA (type NUMBER) (default 0))
    (slot avgTurnLength (type NUMBER) (default 0))
    (slot deltaOfRandomness (type NUMBER) (default 0))
    (slot complexity (type NUMBER) (default 0))
    (slot noOfPlayers (type NUMBER) (default 1)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GAME TYPE MODULES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmodule NEW_PLAYER)
(deftemplate NEW_PLAYER::gameNewPlayer extends GAMEBASE::gameBase
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts NEW_PLAYER::newPlayerGames
	(gameNewPlayer (name "Monopoly") (price 40.0) (EPA 180) (avgTurnLength 5) (deltaOfRandomness 0.7)
				(complexity 0.25) (noOfPlayers 4) (hasCoop FALSE))
	(gameNewPlayer (name "Carcassonne") (price 20.0) (EPA 35) (avgTurnLength 2) (deltaOfRandomness 0.2)
					(complexity 0.4) (noOfPlayers 4) (hasCoop FALSE))
	(gameNewPlayer (name "Augustus") (price 25.0) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.4)
    				(complexity 0.2) (noOfPlayers 4) (hasCoop FALSE))
	(gameNewPlayer (name "Dominion") (price 30.0) (EPA 30) (avgTurnLength 2) (deltaOfRandomness 0.5)
        			(complexity 0.5) (noOfPlayers 4) (hasCoop FALSE))
    (gameNewPlayer (name "Flash Point: Fire Rescue") (price 30.0) (EPA 45) (avgTurnLength 4) (deltaOfRandomness 0.2)
                    (complexity 0.3) (noOfPlayers 4) (hasCoop TRUE)) 

    (gameNewPlayer (name "Puerto Rico") (price 30.0) (EPA 90) (avgTurnLength 6) (deltaOfRandomness 0.4)
            		(complexity 0.5) (noOfPlayers 4) (hasCoop TRUE))
    )

(defrule NEW_PLAYER::recommend
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
	              (wantsToThink ?think) (budget ?bu) (age ?age) (mentalEnergy ?me) (schwifty ?sw))
	(gameNewPlayer (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl) (hasCoop ?hc)
	                (deltaOfRandomness ?delt) (price ?pr))
	(test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
	(test (>= ?nf (- ?noPl 1)))
	(test (<= ?epa ?tp))
	(test (<= ?pr ?bu))
	(test (<= ?comp (RECOMMENDATIONS::maxComplexity ?think ?age ?me)))
	(test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))
	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

(defmodule SOLO)
(deftemplate SOLO::gameSolo extends GAMEBASE::gameBase
	(slot theme (type SYMBOL) (default NONE) (allowed-values NONE Jungle Horror Pirates Fantasy Planes))
	(slot immersion (type NUMBER) (default 0))
	(slot replayability (type NUMBER) (default 0))
	(slot hasExpansions (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts SOLO::soloGames
	(gameSolo (name "Klondike Solitaire") (price 1) (EPA 20) (avgTurnLength 3) (deltaOfRandomness 0.7)
				(complexity 0.3) (theme NONE) (immersion 0.2) (replayability 0.35) (hasExpansions FALSE))
	(gameSolo (name "Arkham Horror") (price 35) (EPA 180) (avgTurnLength 6) (deltaOfRandomness 0.5)
    			(complexity 0.6) (theme Horror) (immersion 0.8) (replayability 0.5) (hasExpansions TRUE))
	(gameSolo (name "Agricola") (price 46) (EPA 150) (avgTurnLength 5) (deltaOfRandomness 0.4)
        		(complexity 0.4) (theme Fantasy) (immersion 0.4) (replayability 0.8) (hasExpansions TRUE))
    (gameSolo (name "DungeonQuest") (price 45) (EPA 50) (avgTurnLength 10) (deltaOfRandomness 0.3)
            	(complexity 0.9) (theme Fantasy) (immersion 1) (replayability 0.6) (hasExpansions TRUE))
    )

(defrule SOLO::recommend
	(gameSolo (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
	            (deltaOfRandomness ?delt) (price ?pr)
	            (theme ?th) (immersion ?imm) (replayability ?repl) (hasExpansions ?hasExp))
    (USER::user (numFriends ?nf) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls) (isCompetitive ?iscomp)
    	        (wantsToThink ?think) (budget ?bu) (age ?age) (mentalEnergy ?me) (schwifty ?sw) (likedThemes $? ?th $?))

	; Generic
	(test (<= ?epa ?tp))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))

    ; Category Specific
    (test (>= ?repl (RECOMMENDATIONS::minReplayability ?bu)))
	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
)

(defmodule TWO_PLAYER)
(deftemplate TWO_PLAYER::gameTwoPlayer extends GAMEBASE::gameBase
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasExpansions (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot replayability (type NUMBER) (default 0))
	(slot rubberbanding (type NUMBER) (default 0)))

(deffacts TWO_PLAYER::twoPlayerGames
	(gameTwoPlayer (name "Magic: The Gathering") (price 200) (EPA 45) (avgTurnLength 4) (deltaOfRandomness 0.4)
					(complexity 1) (hasCoop TRUE) (hasExpansions TRUE) (replayability 0.8) (rubberbanding 0.5))
	(gameTwoPlayer (name "Agricola: All Creatures") (price 32) (EPA 30) (avgTurnLength 5) (deltaOfRandomness 0.2)
    				(complexity 0.4) (hasCoop FALSE) (hasExpansions TRUE) (replayability 0.3) (rubberbanding 0.6))
    (gameTwoPlayer (name "Battle Line") (price 15.5) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.7)
    				(complexity 0.6) (hasCoop FALSE) (hasExpansions FALSE) (replayability 0.5) (rubberbanding 0.5))
    (gameTwoPlayer (name "Hive") (price 99999) (EPA 20) (avgTurnLength 2) (deltaOfRandomness 0.1)
    				(complexity 0.3) (hasCoop FALSE) (hasExpansions TRUE) (replayability 0.8) (rubberbanding 0.3))
	)

(defrule TWO_PLAYER::recommend
	(gameTwoPlayer (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
                   (deltaOfRandomness ?delt) (price ?pr)
                   (replayability ?repl) (hasExpansions ?hasExp) (hasCoop ?coop) (rubberbanding ?rb))
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp)(likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
        	    (wantsToThink ?think) (budget ?bu) (age ?age)(mentalEnergy ?me) (schwifty ?sw))

    ; Generic
    (test (<= ?epa ?tp))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))


    ; Category Specific
    (test (>= ?repl (RECOMMENDATIONS::minReplayability ?bu)))
    (test (or (neq ?comp ?coop)
               (and (eq ?comp TRUE) (>= ?rb 0.4))))
	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

(defmodule CHEAP)

(deftemplate CHEAP::gameCheap extends GAMEBASE::gameBase
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot replayability (type NUMBER) (default 0))
	(slot areComponentsReusable (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasCards (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasBoard (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasDice (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts CHEAP::cheapGames
	(gameCheap (name "Cards Against Humanity") (price 10) (EPA 60) (avgTurnLength 10) (deltaOfRandomness 0.3)
				(complexity 0.2) (hasCoop FALSE) (replayability 0.7) (areComponentsReusable FALSE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	(gameCheap (name "Bohnanza") (price 10) (EPA 45) (avgTurnLength 4) (deltaOfRandomness 0.6)
				(complexity 0.4) (hasCoop FALSE) (replayability 0.4) (areComponentsReusable FALSE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	(gameCheap (name "Zombie Dice") (price 9) (EPA 15) (avgTurnLength 4) (deltaOfRandomness 1)
				(complexity 0.1) (hasCoop FALSE) (replayability 0.2) (areComponentsReusable TRUE)
				(hasCards FALSE) (hasBoard FALSE) (hasDice TRUE))
	(gameCheap (name "Hanabi") (price 10) (EPA 25) (avgTurnLength 4) (deltaOfRandomness 0.5)
				(complexity 0.6) (hasCoop TRUE) (replayability 0.5) (areComponentsReusable FALSE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	(gameCheap (name "Love Letter") (price 5) (EPA 20) (avgTurnLength 2) (deltaOfRandomness 0.6)
				(complexity 0.4) (hasCoop FALSE) (replayability 0.3) (areComponentsReusable TRUE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	)

(defrule CHEAP::recommend
	(gameCheap (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
               (deltaOfRandomness ?delt) (price ?pr)
               (replayability ?repl) (areComponentsReusable ?reuse))
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
            	    (wantsToThink ?think) (budget ?bu) (age ?age)(mentalEnergy ?me) (schwifty ?sw))

    ; Generic
    (test (<= ?epa ?tp))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (>= ?nf (- ?noPl 1)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))

    ; Category Specific
    (test (>= ?repl (RECOMMENDATIONS::minReplayability ?bu)))
    (test (or (eq ?reuse TRUE)
              (eq FALSE (RECOMMENDATIONS::needsToBeReusable ?bu ?age))))
	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

(defmodule PEGI3)
(deftemplate PEGI3::gamePEGI3 extends GAMEBASE::gameBase
	(slot theme (type SYMBOL) (default NONE) (allowed-values NONE Jungle Horror Pirates Fantasy Planes))
	(slot immersion (type NUMBER) (default 0))
	(slot funForParents (type NUMBER) (default 0))
	(slot durability (type NUMBER) (default 0)))

(deffacts PEGI3::PEGI3games
	(gamePEGI3 (name "Catan: Junior") (price 30) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.5)
				(complexity 0.3) (theme Pirates) (immersion 0.4) (funForParents 0.6) (durability 0.5))
	(gamePEGI3 (name "Kids of Carcassonne") (price 25) (EPA 10) (avgTurnLength 2) (deltaOfRandomness 0.2)
    			(complexity 0.3) (theme Fantasy) (immersion 0.4) (funForParents 0.3) (durability 0.6))
    (gamePEGI3 (name "Loopin Louie") (price 23) (EPA 10) (avgTurnLength 3) (deltaOfRandomness 0.8)
    			(complexity 0.2) (theme Planes) (immersion 0.6) (funForParents 0.6) (durability 0.3))
    (gamePEGI3 (name "Monster Factory") (price 24) (EPA 30) (avgTurnLength 6) (deltaOfRandomness 0.4)
    			(complexity 0.4) (theme Horror) (immersion 0.7) (funForParents 0.3) (durability 0.5))
    (gamePEGI3 (name "Gulo Gulo") (price 70) (EPA 20) (avgTurnLength 6) (deltaOfRandomness 0.2)
        		(complexity 0.3) (theme Jungle) (immersion 0.4) (funForParents 0.6) (durability 0.7))
	)

(defrule PEGI3::recommend
	(gamePEGI3 (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
               (deltaOfRandomness ?delt) (price ?pr)
               (theme ?th) (immersion ?imm) (funForParents ?ffp) (durability ?dur))
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
                (wantsToThink ?think) (budget ?bu) (age ?age)(mentalEnergy ?me) (schwifty ?sw) (likedThemes $? ?th $?)
                (wantsDurableComponents ?wantsDurable))

    ; Generic
    (test (<= ?epa ?tp))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (>= ?nf (- ?noPl 1)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))

    ; Category Specific
    (test (>= ?dur (RECOMMENDATIONS::minDurability ?wantsDurable ?bu ?age)))

	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

(defmodule CASUAL)
(deftemplate CASUAL::gameCasual extends GAMEBASE::gameBase
	(slot isPortable (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot setupTime (type NUMBER) (default 0))
	(slot replayability (type NUMBER) (default 0)))

(deffacts CASUAL::casualGames
	(gameCasual (name "Coloretto") (price 8) (EPA 30) (avgTurnLength 2) (deltaOfRandomness 0.6) (complexity 0.3)
    			(isPortable TRUE) (setupTime 1) (replayability 0.7))
	(gameCasual (name "Escape: The Curse of the Temple") (price 60) (EPA 10) (avgTurnLength 4) (deltaOfRandomness 0.6) (complexity 0.4)
        		(isPortable FALSE) (setupTime 4) (replayability 0.5))
    (gameCasual (name "Get Bit!") (price 20) (EPA 20) (avgTurnLength 5) (deltaOfRandomness 0.4) (complexity 0.4)
        		(isPortable FALSE) (setupTime 3) (replayability 0.6))
    (gameCasual (name "King of Tokyo") (price 26) (EPA 30) (avgTurnLength 6) (deltaOfRandomness 0.3) (complexity 0.2)
            	(isPortable FALSE) (setupTime 4) (replayability 0.2))
    )

(defrule CASUAL::recommend
	(gameCasual (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
                (deltaOfRandomness ?delt) (price ?pr)
                (isPortable ?port) (setupTime ?setu) (replayability ?repl))
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
                (wantsToThink ?think) (budget ?bu) (age ?age)(mentalEnergy ?me) (schwifty ?sw) (likedThemes $? ?th $?))

    ; Generic
    (test (<= ?epa (+ ?tp ?setu)))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (>= ?nf (- ?noPl 1)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))

    ; Category Specific
    (test (>= ?repl (RECOMMENDATIONS::minReplayability ?bu)))

	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

(defmodule GROUP)
(deftemplate GROUP::gameGroup extends GAMEBASE::gameBase
	(slot interactivity (type NUMBER) (default 0))
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts GROUP::groupGames
	(gameGroup (name "Bang!") (price 8) (EPA 15) (avgTurnLength 2) (deltaOfRandomness 0.6) (complexity 0.4)
				(interactivity 1) (hasCoop TRUE))
	(gameGroup (name "Werewolf") (price 1) (EPA 60) (avgTurnLength 10) (deltaOfRandomness 0.4) (complexity 0.2)
    			(interactivity 1) (hasCoop TRUE))
    (gameGroup (name "Blood Bound") (price 13) (EPA 30) (avgTurnLength 6) (deltaOfRandomness 0.2) (complexity 0.5)
    			(interactivity 0.7) (hasCoop FALSE))
	(gameGroup (name "Cosmic Encounter") (price 50) (EPA 80) (avgTurnLength 10) (deltaOfRandomness 0.3) (complexity 0.7)
    			(interactivity 0.3) (hasCoop TRUE))
	(gameGroup (name "Dixit") (price 20) (EPA 30) (avgTurnLength 7) (deltaOfRandomness 0.4) (complexity 0.1)
    			(interactivity 1) (hasCoop TRUE))
	)

(defrule GROUP::recommend
	(gameGroup (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
               (deltaOfRandomness ?delt) (price ?pr)
               (hasCoop ?coop) (interactivity ?inter))
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
                (wantsToThink ?think) (budget ?bu) (age ?age)(mentalEnergy ?me) (schwifty ?sw) (likedThemes $? ?th $?))

    ; Generic
    (test (<= ?epa ?tp))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (>= ?nf (- ?noPl 1)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))

    ; Category Specific
    (test (>= ?inter (RECOMMENDATIONS::minInteractivity ?nf (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt))))
	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

(defmodule PARTY)
(deftemplate PARTY::gameParty extends GAMEBASE::gameBase
	(slot durability (type NUMBER) (default 0))
	(slot flirtiness (type NUMBER) (default 0))
	(slot interactivity (type NUMBER) (default 0))
	(slot betterDrunk (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts PARTY::partyGames
	(gameParty (name "Taboo") (price 20) (EPA 20) (avgTurnLength 4) (deltaOfRandomness 0.5) (complexity 0.2)
				(durability 0.5) (flirtiness 0.2) (interactivity 0.6) (betterDrunk FALSE))
	(gameParty (name "Twister") (price 20) (EPA 10) (avgTurnLength 1) (deltaOfRandomness 0.2) (complexity 0.1)
				(durability 0.7) (flirtiness 0.8) (interactivity 1) (betterDrunk TRUE))
	(gameParty (name "Jungle Speed") (price 13) (EPA 10) (avgTurnLength 3) (deltaOfRandomness 0.2) (complexity 0.2)
				(durability 0.7) (flirtiness 0.4) (interactivity 0.8) (betterDrunk TRUE))
	(gameParty (name "Two Rooms and a Boom") (price 1) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.1) (complexity 0.4)
				(durability 0.9) (flirtiness 0.5) (interactivity 1) (betterDrunk FALSE))
	(gameParty (name "Say Anything") (price 16.5) (EPA 30) (avgTurnLength 2) (deltaOfRandomness 0.2) (complexity 0.6)
				(durability 0.6) (flirtiness 0.6) (interactivity 0.7) (betterDrunk FALSE))
	)

(defrule PARTY::recommend
	(gameParty  (name ?name) (complexity ?comp) (avgTurnLength ?avt) (EPA ?epa)(noOfPlayers ?noPl)
                (deltaOfRandomness ?delt) (price ?pr)
                (durability ?dur) (interactivity ?inter))
	(USER::user (numFriends ?nf) (isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
                (wantsToThink ?think) (budget ?bu) (age ?age)(mentalEnergy ?me) (schwifty ?sw) (likedThemes $? ?th $?)
                (wantsDurableComponents ?wantsDurable))

    ; Generic
    (test (<= ?epa ?tp))
    (test (<= ?pr ?bu))
    (test (<= ?avt (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (>= ?nf (- ?noPl 1)))
    (test (<= ?delt (RECOMMENDATIONS::maxDelta ?ls ?think)))

    ; Category Specific
    (test (>= ?inter (RECOMMENDATIONS::minInteractivity ?nf (RECOMMENDATIONS::maxTurnLength ?lw ?tp ?epa ?avt))))
    (test (>= ?dur (RECOMMENDATIONS::minDurability ?wantsDurable ?bu ?age)))

	=>
	(assert (RECOMMENDATIONS::recommendedGame (name ?name)))
	)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAIN MODULE, TO TRIGGER THE MATCHING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmodule myMAIN)

(defrule myMAIN::newPlayerConditions
    (USER::user {howLongPlaying < 2})
    =>
    (printout t "We are looking in NEW PLAYER games." crlf)
    (focus NEW_PLAYER)
)

(defrule myMAIN::soloConditions
    (USER::user {numFriends == 0})
    =>
    (printout t "We are looking in SOLO games." crlf)
    (focus SOLO)
)

(defrule myMAIN::twoPlayerConditions
    (USER::user {numFriends == 1})
    =>
    (printout t "We are looking in TWO_PLAYER games." crlf)
    (focus TWO_PLAYER)
)

(defrule myMAIN::cheapConditions
    (USER::user {budget <= 20})
    =>
    (printout t "We are looking in CHEAP games." crlf)
    (focus CHEAP)
)

(defrule myMAIN::PEGI3Conditions
	(USER::user {age < 10})
	=>
	(printout t "We are looking in PEGI3 games." crlf)
	(focus PEGI3)
)

(defrule myMAIN::casualConditions
    (USER::user {timeToPlay <= 60} {wantsToThink != TRUE})
    =>
    (printout t "We are looking in CASUAL games." crlf)
    (focus CASUAL)
)

(defrule myMAIN::groupConditions
    (USER::user {numFriends > 5})
    =>
    (printout t "We are looking in GROUP games." crlf)
    (focus GROUP)
)

(defrule myMAIN::partyConditions
    (USER::user {schwifty == TRUE} (numFriends ?np))
    (test (> ?np 5))
    =>
    (printout t "We are looking in PARTTY games." crlf)
    (focus PARTY)
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(defmodule USER)
;(deftemplate USER::user
;	(slot age (type NUMBER))
;   (slot howLongPlaying (type NUMBER))
;	(slot numFriends (type NUMBER) (default 0))
;	(slot budget (type NUMBER) (default 0))
;	(slot mentalEnergy (type NUMBER) (default 0))
;	(slot timeToPlay (type NUMBER) (default 0))
;	(slot isCompetitive (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
;	(slot likesToWait (type NUMBER) (default 1.0) (allowed-values 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0))
;	(slot likesSurprises (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
;	(slot wantsToThink (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
;	(slot schwifty (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
;   (slot wantsDurableComponents (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
;   (multislot likedThemes (type SYMBOL) (default NONE Jungle Horror Pirates Fantasy Planes)
;                                        (allowed-values NONE Jungle Horror Pirates Fantasy Planes))
;	)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
