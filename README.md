# AI LABS

## LABs 5, 7 & 8 

Don't even bother with Jess. Trust us, you don't want to go there.

## LAB 10

### Run the solvers

Create a run configuration with the CirclesWindow.java file as the main class. 

### Console commands

> ***Note: *** As of now, you need *all* these commands *in that  order* for the system to pick them up properly.

```bash
    $> <compiler> <program> -g -sB (Brute Force solver)
    $> <compiler> <program> -g -sR (Random solver)
    $> <compiler> <program> -g -sG (Genetic solver)
```





