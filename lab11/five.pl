% Sentences to recognize:
% 
% Note that some acccents and uppercase letters have been replaced.
% 
% ¿Cuál es el salario de garcia? ¿Cuál es la edad de garcia?
% Dime el departamento de martinez
% ¿Qué salario tiene martinez? ¿Qué edad tiene martinez?
% Dame los nombres de los empleados del departamento de ventas.
% Quiero saber los nombres de los empleados de la factoria de Madrid.
  
% Main interface: Requests for the input and returns the result.
% Query: interface.
% Example of correct query: 'Cuál es la edad de garcia?' (with quotes)
interface :- 
    write('Escribe tu consulta (sin mayúsculas ni tildes, entre apóstrofes): '),
    read(Input),
    % Split the consult into a lsit of words.
    atomic_list_concat(Consult, ' ', Input), 
    % Parse the sentence and execute the consult.
    consulta(Consult, Result), 
    write('Aquí tienes los resultados (';' + enter para continuar): '),
    write(Result).

% Different types of consult: 
% Ask for the names of employees that have a certain attribute at a certain value
consulta(Consulta, Resultado) :- names_query(Resultado, Consulta, _).

% Ask for a certain value of a certain employee's attribute.
consulta(Consulta, Resultado) :- value_query(Resultado, Consulta, _).

% Intermediate parsers:

% Requesting all the names that fulfill a certain attribute/value pair
names_query(Result) -->
    filler,
    names_request,
    group_pronoun(Gender),
    attribute(Attribute, Gender),
    filler,
   	value(Attribute, Value),
    {names_retrieve(Attribute, Value, Result)}.

names_query(Result) -->
    filler, filler,
    names_request,
    group_pronoun(Gender),
    attribute(Attribute, Gender),
    filler,
   	value(Attribute, Value),
    {names_retrieve(Attribute, Value, Result)}.


% Different variants for requesting a value
value_query(Result) --> 
    pronoun(Gender),
    attribute(Attribute, Gender), 
    filler,
    name(Name), 
    {value_retrieve(Name, Attribute, Result)}. 

value_query(Result) --> 
    filler,
    pronoun(Gender),
    attribute(Attribute, Gender), 
    filler,
    name(Name), 
    {value_retrieve(Name, Attribute, Result)}.

value_query(Result) --> 
    filler,
    filler,
    pronoun(Gender),
    attribute(Attribute, Gender), 
    filler,
    name(Name), 
    {value_retrieve(Name, Attribute, Result)}.

value_query(Result) --> 
    filler,
    filler,
    filler, 
    pronoun(Gender),
    attribute(Attribute, Gender), 
    filler,
    name(Name), 
    {value_retrieve(Name, Attribute, Result)}.

value_query(Result) --> 
    enquiry,
    attribute(Attribute, _), 
    filler,
    name(Name), 
    {value_retrieve(Name, Attribute, Result)}.


% ----------------------
% Atomic
% ----------------------

% A word not relevant to the consult
filler --> [Word],
   { empleado(N, A, _),
     Word \= A,
     Word \= N
   }.

% Formato de petición de nombres
names_request --> [A, B, C, D, E],
    { atomic_list_concat([A, B, C, D, E], ' ', R),
      R == 'los nombres de los empleados'
    }.  

% A group pronoun is either a composed prooun (del) or a determinant and a pronoun (de la).
group_pronoun(Gender) --> pronoun(Gender).
group_pronoun(Gender) --> filler, pronoun(Gender).    
 
% Pronouns: el, la, los, las...
pronoun(Gender) --> [Word],    
   { is_gender(Gender, Word),
   	 empleado(N, A, _),     
     Word \= A,
     Word \= N
   }.

enquiry --> [W], {
    atom_concat(Punctuation, Name, W),
    is_punctuation(Punctuation),
    is_question(Name)
    }.          

% Given a gender, return the attribute (A) if at least one employee exists with
% that attribute.
% It recognizes both singular and plural attributes.
attribute(Attr, Gender) --> [A],    
    { atom_concat(Attr, Plural, A),
      is_plural_mark(Plural),
      is_gender(Gender, Attr),
      empleado(_, Attr, _) % There is at least one employee with that attribute
    }.

% Given a word, try to unify it with the name of at least one employee.
% Skip punctuation such as ., ?, etc.
name(Name) --> [N], {
	atom_concat(Name, Punctuation, N),
    is_punctuation(Punctuation),
    empleado(Name, _, _)
    }.

% A value is unified if there is at least one employee with that attribute/value combination
value(Attribute, Value) --> [V], {
	atom_concat(Value, Punctuation, V),
    is_punctuation(Punctuation),
    empleado(_, Attribute, Value)
    }.

% ----------------------
% DB Queries
% ----------------------

% Standard Prolog to return the value of the consult.
% Assumes Result will be unique.
value_retrieve(Name, Attribute, Result) :- empleado(Name, Attribute, Result).

% Matches into Result all the names that fulfill the attribute/value pair
names_retrieve(Attribute, Value, Result) :- empleado(Result, Attribute, Value).

% ----------------------
% Knowledge
% ----------------------
empleado(garcia, salario, '2300').
empleado(garcia, edad, '42').
empleado(garcia, departamento, 'marketing').
empleado(garcia, factoria, 'Madrid').
empleado(martinez, salario, '756').
empleado(martinez, edad, '23').
empleado(martinez, departamento, 'ventas').
empleado(martinez, factoria, 'Valencia').

% Punctuation
is_punctuation(?).
is_punctuation(¿).
is_punctuation(.).
is_punctuation('').

% Pronouns
is_gender(masculino, el).
is_gender(femenino, la).
is_gender(masculino, los).
is_gender(femenino, las).
 % Composed pronouns
is_gender(masculino, del).
% Attributes
is_gender(femenino, edad).
is_gender(masculino, salario).
is_gender(masculino, departamento).

% Plural. No miramos la concordancia, por lo que 
% el segundo hecho también se acepta.
is_plural_mark(s).
is_plural_mark('').

is_question('Qué').
is_question('que').