% Execution samples
% phrase('El niño dibujó las flores', X).
% phrase('La artista dibujó las flores en el cuaderno', X).
% phrase('Yo tomé la decisión', X).


% We strip all capitalization and accents
phrase(X, Result) :- downcase_atom(X,Y), unaccent_atom(Y,Z), 
                     tokenize_atom(Z,P), 
                     phrase_aux(Voice,Verb,Do,Adv,Subj,Per,P,[]), 
                     passive(Verb,Do,Voice,Adv,Subj,Per,Result).

passive(Verb, DirObj, Voice, Adv, Subj, Per, Rslt) :- eqvl_voice(Per,EqV,EqT),
                                            atom_concat(DirObj,EqV,Start), 
                                            atom_concat(Verb,EqT,PassVrb),
                                            atom_concat(Start,PassVrb,FstHalf),
                                            eqvl_pronoun(Voice, Subj, Eq),
                                            atom_concat(FstHalf,Eq,Whole),
                                            atom_concat(Whole,Adv,Rslt).

% A phrase is basically a noun followed by a verbal group
phrase_aux(Voice, Root, DirObj, Adv, Subj, Per) --> noun_group(Subj,_), 
                                                    verbal_group(Voice,Root,DirObj,Adv,Per).

% We get the person of the noun group from the article
noun_group(NG, none) --> noun(NG).
noun_group(NG, Person) --> article(A,Person), noun(N), {atom_concat(A, N, NG)}. 

% Verbal groups don't need an adverbial
verbal_group(Voice, Root, DirObj, '', Per) --> verb(_,Root,Voice), 
                                               direct_object(DirObj,Per).
verbal_group(Voice, Root, DirObj, Adv, Per) --> verb(C,Root,Voice), 
                                                direct_object(DirObj,Per), 
                                                adverbial(C,Adv).

adverbial([], '') --> [].
adverbial([C], Adv) --> preposition(C), noun_group(NG,_), 
                        {atom_concat(C,' ',Pre), atom_concat(Pre,NG,Adv)}.

direct_object(NG, Person) --> noun_group(NG,Person).

noun(N) --> [P], {is_name(P), atom_concat(P,' ',N)}.

verb(Compl, Root, Voice) --> [Verb], {atom_concat(Root,Term,Verb), 
                                      is_verb(Root,Compl), 
                                      is_termination(Term,Voice)}.

preposition(P) --> [P], {is_preposition(P)}.

quantifier --> [Q], {is_quantifier(Q)}.

article(Art, Person) --> [A], {is_article(A,Person), atom_concat(A, ' ', Art)}.

% Dictionary
is_name(nino). % We don't check concordancy between noun and verb, we should
is_name(artista).
is_name(flor). % but there wasn't enough time.
is_name(flores).
is_name(cuaderno).
is_name(yo). 
is_name(decision).
is_name(conejo).
is_name(conejos).

is_article(el,sing_male).
is_article(la,sing_fema).
is_article(los,plur_male).
is_article(las,plur_fema).

is_preposition(en).

is_quantifier(una).

is_verb(dibuj, [en]). % We can allow a single preposition
is_verb(ser, []). % No preposition at all
is_verb(tom, [_]). % Or all prepositions

is_termination(e, first). % Second parameter indicates verb person usage
is_termination(o, third).

% Passive Equivalents
eqvl_pronoun(first, _, mi).
eqvl_pronoun(third, Subj, Subj). % If subj is third person we echo it

eqvl_voice(sing_male, 'fue ', 'ado por ').
eqvl_voice(sing_fema, 'fue ', 'ada por ').
eqvl_voice(plur_male, 'fueron ', 'ados por ').
eqvl_voice(plur_fema, 'fueron ', 'adas por ').
