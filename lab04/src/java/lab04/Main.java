package lab04;

import jess.*;

public class Main {

	public static void main(String[] args) throws JessException {
		String filePath = System.getProperty("user.dir");
		Rete engine = new Rete();
		if(!filePath.contains("lab04"))
			filePath += "/lab04";

		engine.batch(filePath + "/src/jess/familia0.clp");
	}
}
