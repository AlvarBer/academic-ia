(deffacts inicio
    (dd juan maria rosa m)
    (dd juan maria luis h)
    (dd jose laura pilar m)
    (dd luis pilar miguel h)
    (dd miguel isabel jaime h))

(defrule padre
   (dd ?x ? ?y ?)
    =>
   (assert (padre ?x ?y)))

(defrule madre
  (dd ? ?x ?y ?)
   =>
   (assert (madre ?x ?y)))

(defrule hijo
  (dd ?x ?y ?z ?t)
  (test (eq ?t h))
   =>
   (assert (hijo ?z ?x))
   (assert (hijo ?z ?y)))

(defrule hija
  (dd ?x ?y ?z ?t)
  (test (eq ?t m))
   =>
   (assert (hija ?z ?x))
   (assert (hija ?z ?y)))

;; Check if t1 is hermano de t2
(defrule hermano
  (dd ?x1 ?y1 ?z1 ?t1)
  (dd ?x2 ?y2 ?z2 ?t2)
  (test (eq ?x1 ?x2))
  (test (eq ?y1 ?y2))
  (test (eq ?t1 h))
  (test (neq ?z1 ?z2))
   =>
  (assert (hermano ?z1 ?z2)))


;; Check if t1 es hermana de t2
(defrule hermana
    (dd ?x1 ?y1 ?z1 ?t1)
    (dd ?x2 ?y2 ?z2 ?t2)
    (test (eq ?x1 ?x2))
    (test (eq ?y1 ?y2))
    (test (eq ?t1 m))
    (test (neq ?z1 ?z2))
    =>
    (assert (hermana ?z1 ?z2)))


(defrule abuelo
    (padre ?x1 ?x2)
    (or (padre ?x2 ?x3)
        (madre ?x2 ?x3))
    =>
    (assert (abuelo ?x1 ?x3)))

(defrule abuela
    (madre ?x1 ?x2)
     (or (padre ?x2 ?x3)
         (madre ?x2 ?x3))
     =>
    (assert (abuela ?x1 ?x3)))

(defrule primo
    (or (madre ?x1 ?x2)
        (padre ?x1 ?x2))
    (or (hermano ?x1 ?h1)
        (hermana ?x1 ?h1))
    (hijo ?p1 ?h1)

    =>
    (assert (primo ?p1 ?x2)))

(defrule prima
     (or (madre ?x1 ?x2)
         (padre ?x1 ?x2))
     (or (hermano ?x1 ?h1)
         (hermana ?x1 ?h1))
     (hija ?p1 ?h1)

      =>
     (assert (prima ?p1 ?x2)))

(defrule ascendiente
      (or (or (madre ?x1 ?x2)(padre ?x1 ?x2))
          (and (ascendiente ?x0 ?x1) (or (madre ?x1 ?x2) (padre ?x1 ?x2))))

      =>
     (assert (ascendiente ?x1 ?x2)))

(reset)
(run)
(facts)
