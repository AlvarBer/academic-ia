% Grupo 5: Borja Lorente, Alvaro Bermejo

% Initial Facts
dd(juan, maria, rosa, m).
dd(juan, maria, luis, h).
dd(jose, laura, pilar, m).
dd(luis, pilar, miguel, h).
dd(miguel,isabel,jaime,h).
dd(pedro, rosa, pablo, h).
dd(pedro, rosa, begona, m).

% Mother - Father Relationships
padre(X, Y):- dd(X, _, Y, _).
madre(X, Y):- dd(_, X, Y, _).
progenitor(X, Y):- padre(X, Y).
progenitor(X, Y):- madre(X, Y).

/* Hij@s */
hijo(X, Y):- dd(Y, _, X, h).
hijo(X, Y):- dd(_, Y, X, h).
hija(X, Y):- dd(Y, _, X, m).
hija(X, Y):- dd(_, Y, X, m).

/* Herman@s */
hermano(X, Y):- dd(PADRE, MADRE, X, h), dd(PADRE, MADRE, Y, _), X\=Y.
hermana(X, Y):- dd(PADRE, MADRE, X, m), dd(PADRE, MADRE, Y, _), X\=Y.
hermanos(X, Y):- dd(PADRE, MADRE, X, _), dd(PADRE, MADRE, Y, _), X\=Y.

/* Abuel@s */
abuelo(X, Y):- padre(X, T), progenitor(T, Y).
abuela(X, Y):- madre(X, T), progenitor(T, Y).

/* Prim@s */
primo(X, Y):- progenitor(P1, X), progenitor(P2, Y), hermano(P1, P2), dd(_, _, X, h).
primo(X, Y):- progenitor(P1, X), progenitor(P2, Y), hermana(P1, P2), dd(_, _, X, h).

primos(X, Y):- progenitor(P1, X), progenitor(P2, Y), hermanos(P1, P2), dd(_, _, X, _).

prima(X, Y):- progenitor(P1, X), progenitor(P2, Y), hermano(P1, P2), dd(_, _, X, m).
prima(X, Y):- progenitor(P1, X), progenitor(P2, Y), hermana(P1, P2), dd(_, _, X, m).

/* Ascendiente */
ascendiente(X, Y):- progenitor(X, Y).
ascendiente(X, Y):- progenitor(X, P), ascendiente(P, Y).