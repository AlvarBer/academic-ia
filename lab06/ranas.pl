
%
Initial & Objective State
initial(state(n,n,n,h,b,b,b)).

objective(state(X1, X2, X3, _, Y1, Y2, Y3)) :-
	X1\=n, X2\=n, X3\=n, Y1\=b, Y2\=b, Y3\=b.

% state([X1, X2, X3, X4 ,X5 , X6, X7]) :- state(X1, X2, X3, X4, X5, X6,
% X7). state(X1, X2, X3, X4, X5, X6, X7) :- state([X1, X2, X3, X4, X5,
% X6, X7]). state([X, state([Y|R])]) :- state([X, Y|R]), print(R).

can(State,_,[]) :- objective(State),print(State).
can(State, Visited, [Operator |Operators]) :-
        movement(State,NextState,Operator),
	print([NextState | Visited]),
	print(Operator),
	print(NextState), nl,
        \+member(NextState, Visited),
	can(NextState, [NextState | Visited], Operators).

check :- initial(State), can(State, Visited, Operators) , nl,
	write('Found solution without state repetition: '), nl,
	write(Operators), nl, write('Path States: '), nl, write(Visited).

% Operators
% Left Operators
operation(move0Right,[h,X|R],[X,h|R]). % Base case
operation(move0Right,[X,Y|R1],[X|R2]) :- operation(move0Right, [Y|R1], R2).

operation(move1Right,[h,X,Y|R],[Y,X,h|R]). % Base case
operation(move1Right,[X,Y,Z|R1],[X|R2]) :- operation(move1Right, [Y,Z|R1], R2).

operation(move2Right,[h,X,Y,Z|R],[X,Y,Z,h|R]). % Base case
operation(move2Right,[X,Y,Q,Z|R1],[X|R2]) :- operation(move2Right, [Y,Q,Z|R1], R2).

operation(move0Left,[X,h|R],[h,X|R]). % Base case
operation(move0Left,[X,Y|R1],[X|R2]) :- operation(move0Left, [Y|R1], R2).

operation(move1Left,[X,Y,h|R],[h,Y,X|R]). % Base case
operation(move1Left,[X,Y,Z|R1],[X|R2]) :- operation(move1Left, [Y,Z|R1], R2).

operation(move2Left,[Z,X,Y,h|R],[h,X,Y,Z|R]). % Base case
operation(move2Left,[X,Y,Q,Z|R1],[X|R2]) :- operation(move2Left, [Y,Q,Z|R1], R2).

movement(state(X1,X2,X3,X4,X5,X6,X7), state(Z1,Z2,Z3,Z4,Z5,Z6,Z7), O) :-
	operation(O, [X1,X2,X3,X4,X5,X6,X7], [Z1,Z2,Z3,Z4,Z5,Z6,Z7]).


