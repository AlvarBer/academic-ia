% Frases a reconocer:
%
% David habla con Ana. 
% Julia lee libros en el jardín. 
% Los niños leen. 
% Pedro juega. 
% Elvira juega en la piscina.

frase --> grupo_nominal(Numero), grupo_verbal(Numero, Preposicion).
grupo_verbal(Numero, Preposicion) --> verbo(_, _, _, Numero, Preposicion).
%grupo_verbal(Numero) --> verbo(_, _, _, Numero), complemento_circunstancial.
grupo_nominal(_) --> nombre_propio.
grupo_nominal(Numero) --> determinante(Numero, Sexo), sustantivo(Numero, Sexo).

% -----------------------------------
% Reglas Atomicas:
% -----------------------------------
determinante(Numero, Sexo) --> [D],
	{
		es_determinante(D, Numero, Sexo)
	}.

sustantivo(Numero, Sexo) --> [S],
	{
		atom_concat(Lexema, Morfema, S),
		es_lexema(Lexema),
		es_morfema(Morfema, Numero, Sexo)
	}.	

verbo(Infinitivo, Tiempo, Persona, Numero, Preposicion) --> [V],
	{
		atom_concat(Raiz, Terminacion, V), % Busca automaticamente en el diccionario. 
	 									    % Pueden ser cadenas vacías.
										    % No nos tenemos que preocupar por su longitud.
		es_raiz(Raiz, Infinitivo, Conjugacion, Preposicion),
		es_terminacion(Terminacion, Tiempo, Persona, Numero, Conjugacion)
	}.


% -----------------------------------
% Diccionario:
% -----------------------------------
nombre_propio --> [david].
nombre_propio --> [julia].
nombre_propio --> [pedro].
nombre_propio --> [elvira].

es_determinante(el, singular, masculino).
es_determinante(la, singular, femenino).
es_determinante(los, plural, masculino).
es_determinante(las, plural, femenino).

es_lexema(nin).
es_lexema(embarazad).

es_morfema(o, singular, masculino).
es_morfema(a, singular, femenino).
es_morfema(os, plural, masculino).
es_morfema(as, plural, femenino).

es_raiz(habl, hablar, primera, con).
es_raiz(cant, cantar, primera, bajo).
es_raiz(jueg, jugar, primera, con).
es_raiz(le, leer, segunda, en).
% Primera conjugacion
es_terminacion(o, presente, primera, singular, primera).
es_terminacion(as, presente, segunda, singular, primera).
es_terminacion(a, presente, tercera, singular, primera).
es_terminacion(amos, presente, primera, plural, primera).
es_terminacion(ais, presente, segunda, plural, primera).
es_terminacion(an, presente, tercera, plural, primera).
% Segunda conjugacion
es_terminacion(o, presente, primera, singular, segunda).
es_terminacion(es, presente, segunda, singular, segunda).
es_terminacion(e, presente, tercera, singular, segunda).
es_terminacion(emos, presente, primera, plural, segunda).
es_terminacion(eis, presente, segunda, plural, segunda).
es_terminacion(en, presente, tercera, plural, segunda).

