preguntaVerbo :-
	read(V),
	verbo(I, T, P, N, [V], []),
	write(I),write(T),write(P),write(N).  

preguntaConcuerda :-
	write("Pronombre: "), read(Pronombre),
	write("Verbo: "), read(Verbo),
	concuerda([Pronombre, Verbo], []).


concuerda --> 
	pronombre(Persona, Numero),
	verbo(_, _, Persona, Numero).
	
% Reconocimiento de verbos
% e.g.
% Infinitivo: Hablar
% 
% Otras formas a reconocer: Hablará, Hablas...
% Lexema: Habl
verbo(Infinitivo, Tiempo, Persona, Numero) --> [V],
	{
		atom_concat(Raiz, Terminacion, V), % Busca automaticamente en el diccionario. 
	 									    % Pueden ser cadenas vacías.
										    % No nos tenemos que preocupar por su longitud.
		es_raiz(Raiz, Infinitivo),
		es_terminacion(Terminacion, Tiempo, Persona, Numero)
	}.

% Reconocimiento de pronombres
pronombre(Persona, Numero) --> [P],
	{
		es_pronombre(P, Persona, Numero)
	}.

% Diccionario
es_raiz(habl, hablar).
es_raiz(cant, cantar).

es_terminacion(o, presente, primera, singular).
es_terminacion(as, presente, segunda, singular).
es_terminacion(a, presente, tercera, singular).
es_terminacion(amos, presente, primera, plural).
es_terminacion(ais, presente, segunda, plural).
es_terminacion(an, presente, tercera, plural).

es_pronombre(yo, primera, singular).
es_pronombre(tu, segunda, singular).
es_pronombre(el, tercera, singular).
es_pronombre(ella, tercera, singular).
