; Group 5
; Álvaro Bermejo García
; Borja Lorente Escobar

/* (batch C:/hlocal/academic-ia/lab7/lab7.clp) */
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Protégé Classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;(mapclass LOCATION)
;(mapclass STORE)
;(mapclass PUBLISHER)
;(mapclass DESIGNER)
;(mapclass GAME)
;(mapclass MYUSER)

(reset)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Jess Facts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftemplate gameBase
    (slot name (type STRING) (default "Game"))
    (slot price (type FLOAT) (default 0))
    (slot EPA (type NUMBER) (default 0))
    (slot avgTurnLength (type NUMBER) (default 0))
    (slot deltaOfRandomness (type FLOAT) (default 0))
    (slot complexity (type SYMBOL) (default MEDIUM) (allowed-values LOW MEDIUM HIGH))
    (slot noOfPlayers (type NUMBER) (default 1))
    (slot pub_name (type STRING) ) 
    (slot des_name (type STRING) (default "James Portnow"))
    (multislot store_names (type STRING) (default "Akira Games"))
)


(deftemplate gameNewPlayer extends gameBase
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts newPlayerGames
	(gameNewPlayer (name "Monopoly") (price 40.0) (EPA 180) (avgTurnLength 5) (deltaOfRandomness 0.7)
				(complexity LOW) (noOfPlayers 4) (hasCoop FALSE))
	(gameNewPlayer (name "Carcassonne") (price 20.0) (EPA 35) (avgTurnLength 2) (deltaOfRandomness 0.2)
					(complexity MEDIUM) (noOfPlayers 4) (hasCoop FALSE))
	(gameNewPlayer (name "Augustus") (price 25.0) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.4)
    				(complexity LOW) (noOfPlayers 4) (hasCoop FALSE))
	(gameNewPlayer (name "Dominion") (price 30.0) (EPA 30) (avgTurnLength 2) (deltaOfRandomness 0.5)
        			(complexity MEDIUM) (noOfPlayers 4) (hasCoop FALSE))
    (gameNewPlayer (name "Flash Point: Fire Rescue") (price 30.0) (EPA 45) (avgTurnLength 4) (deltaOfRandomness 0.2)
                    (complexity LOW) (noOfPlayers 4) (hasCoop TRUE)) 
    (gameNewPlayer (name "Puerto Rico") (price 30.0) (EPA 90) (avgTurnLength 6) (deltaOfRandomness 0.4)
            		(complexity MEDIUM) (noOfPlayers 4) (pub_name "Holocubierta") (hasCoop TRUE))
    )

(deftemplate gameSolo extends gameBase
	(slot theme (type SYMBOL) (default NONE) (allowed-values NONE Jungle Horror Pirates Fantasy Planes))
	(slot immersion (type FLOAT) (default 0))
	(slot replayability (type FLOAT) (default 0))
	(slot hasExpansions (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts soloGames
	(gameSolo (name "Klondike Solitaire") (price 1.0) (EPA 20) (avgTurnLength 3) (deltaOfRandomness 0.7) (pub_name "Holocubierta")
				(complexity LOW) (theme NONE) (immersion 0.2) (replayability 0.35) (hasExpansions FALSE))
	(gameSolo (name "Arkham Horror") (price 35.0) (EPA 180) (avgTurnLength 6) (deltaOfRandomness 0.5) (pub_name "Holocubierta")
    			(complexity MEDIUM) (theme Horror) (immersion 0.8) (replayability 0.5) (hasExpansions TRUE) )
	(gameSolo (name "Agricola") (price 46.0) (EPA 150) (avgTurnLength 5) (deltaOfRandomness 0.4) 
        		(complexity MEDIUM) (theme Fantasy) (immersion 0.4) (replayability 0.8) (hasExpansions TRUE))
    (gameSolo (name "DungeonQuest") (price 45.0) (EPA 50) (avgTurnLength 10) (deltaOfRandomness 0.3) (pub_name "Holocubierta")
            	(complexity HIGH) (theme Fantasy) (immersion 1.0) (replayability 0.6) (hasExpansions TRUE))
    )

(deftemplate gameTwoPlayer extends gameBase
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasExpansions (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot replayability (type FLOAT) (default 0))
	(slot rubberbanding (type FLOAT) (default 0)))

(deffacts twoPlayerGames
	(gameTwoPlayer (name "Magic: The Gathering") (price 200.0) (EPA 45) (avgTurnLength 4) (deltaOfRandomness 0.4) (pub_name "Holocubierta")
					(complexity HIGH) (hasCoop TRUE) (hasExpansions TRUE) (replayability 0.8) (rubberbanding 0.5))
	(gameTwoPlayer (name "Agricola: All Creatures") (price 32.0) (EPA 30) (avgTurnLength 5) (deltaOfRandomness 0.2) (pub_name "Holocubierta")
    				(complexity MEDIUM) (hasCoop FALSE) (hasExpansions TRUE) (replayability 0.3) (rubberbanding 0.6))
    (gameTwoPlayer (name "Battle Line") (price 15.5) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.7) (pub_name "Holocubierta")
    				(complexity MEDIUM) (hasCoop FALSE) (hasExpansions FALSE) (replayability 0.5) (rubberbanding 0.5))
    (gameTwoPlayer (name "Hive") (price 99999.0) (EPA 20) (avgTurnLength 2) (deltaOfRandomness 0.1) (pub_name "Holocubierta")
    				(complexity LOW) (hasCoop FALSE) (hasExpansions TRUE) (replayability 0.8) (rubberbanding 0.3))
	)

(deftemplate gameCheap extends gameBase
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot replayability (type FLOAT) (default 0))
	(slot areComponentsReusable (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasCards (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasBoard (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot hasDice (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts cheapGames
	(gameCheap (name "Cards Against Humanity") (price 10.0) (EPA 60) (avgTurnLength 10) (deltaOfRandomness 0.3) (pub_name "Holocubierta")
				(complexity LOW) (hasCoop FALSE) (replayability 0.7) (areComponentsReusable FALSE) 
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	(gameCheap (name "Bohnanza") (price 10.0) (EPA 45) (avgTurnLength 4) (deltaOfRandomness 0.6) (pub_name "Holocubierta")
				(complexity MEDIUM) (hasCoop FALSE) (replayability 0.4) (areComponentsReusable FALSE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	(gameCheap (name "Zombie Dice") (price 9.0) (EPA 15) (avgTurnLength 4) (deltaOfRandomness 1.0) (pub_name "Holocubierta")
				(complexity LOW) (hasCoop FALSE) (replayability 0.2) (areComponentsReusable TRUE)
				(hasCards FALSE) (hasBoard FALSE) (hasDice TRUE))
	(gameCheap (name "Hanabi") (price 10.0) (EPA 25) (avgTurnLength 4) (deltaOfRandomness 0.5) (pub_name "Holocubierta")
				(complexity MEDIUM) (hasCoop TRUE) (replayability 0.5) (areComponentsReusable FALSE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	(gameCheap (name "Love Letter") (price 5.0) (EPA 20) (avgTurnLength 2) (deltaOfRandomness 0.6) (pub_name "Holocubierta")
				(complexity MEDIUM) (hasCoop FALSE) (replayability 0.3) (areComponentsReusable TRUE)
				(hasCards TRUE) (hasBoard FALSE) (hasDice FALSE))
	)

(deftemplate gamePEGI3 extends gameBase
	(slot theme (type SYMBOL) (default NONE) (allowed-values NONE Jungle Horror Pirates Fantasy Planes))
	(slot immersion (type FLOAT) (default 0))
	(slot funForParents (type FLOAT) (default 0))
	(slot durability (type FLOAT) (default 0)))

(deffacts PEGI3games
	(gamePEGI3 (name "Catan: Junior") (price 30.0) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.5) (pub_name "Holocubierta")
				(complexity LOW) (theme Pirates) (immersion 0.4) (funForParents 0.6) (durability 0.5))
	(gamePEGI3 (name "Kids of Carcassonne") (price 25.0) (EPA 10) (avgTurnLength 2) (deltaOfRandomness 0.2) (pub_name "Holocubierta")
    			(complexity LOW) (theme Fantasy) (immersion 0.4) (funForParents 0.3) (durability 0.6))
    (gamePEGI3 (name "Loopin Louie") (price 23.0) (EPA 10) (avgTurnLength 3) (deltaOfRandomness 0.8) (pub_name "Holocubierta")
    			(complexity LOW) (theme Planes) (immersion 0.6) (funForParents 0.6) (durability 0.3))
    (gamePEGI3 (name "Monster Factory") (price 24.0) (EPA 30) (avgTurnLength 6) (deltaOfRandomness 0.4) (pub_name "Holocubierta")
    			(complexity MEDIUM) (theme Horror) (immersion 0.7) (funForParents 0.3) (durability 0.5))
    (gamePEGI3 (name "Gulo Gulo") (price 70.0) (EPA 20) (avgTurnLength 6) (deltaOfRandomness 0.2) (pub_name "Holocubierta")
        		(complexity LOW) (theme Jungle) (immersion 0.4) (funForParents 0.6) (durability 0.7))
	)

(deftemplate gameCasual extends gameBase
	(slot isPortable (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE))
	(slot setupTime (type FLOAT) (default 0))
	(slot replayability (type FLOAT) (default 0)))

(deffacts casualGames
	(gameCasual (name "Coloretto") (price 8.0) (EPA 30) (avgTurnLength 2) (deltaOfRandomness 0.6) (complexity LOW)
    			(isPortable TRUE) (setupTime 1) (replayability 0.7) (pub_name "Holocubierta"))
	(gameCasual (name "Escape: The Curse of the Temple") (price 60.0) (EPA 10) (avgTurnLength 4) (deltaOfRandomness 0.6) (complexity MEDIUM)
        		(isPortable FALSE) (setupTime 4) (replayability 0.5) (pub_name "Holocubierta"))
    (gameCasual (name "Get Bit!") (price 20.0) (EPA 20) (avgTurnLength 5) (deltaOfRandomness 0.4) (complexity MEDIUM)
        		(isPortable FALSE) (setupTime 3) (replayability 0.6) (pub_name "Holocubierta"))
    (gameCasual (name "King of Tokyo") (price 26.0) (EPA 30) (avgTurnLength 6) (deltaOfRandomness 0.3) (complexity LOW)
            	(isPortable FALSE) (setupTime 4) (replayability 0.2) (pub_name "Holocubierta"))
    )

(deftemplate gameGroup extends gameBase
	(slot interactivity (type FLOAT) (default 0))
	(slot hasCoop (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts groupGames
	(gameGroup (name "Bang!") (price 8.0) (EPA 15) (avgTurnLength 2) (deltaOfRandomness 0.6) (complexity MEDIUM)
				(interactivity 1.0) (hasCoop TRUE) (pub_name "Holocubierta"))
	(gameGroup (name "Werewolf") (price 10.0) (EPA 60) (avgTurnLength 10) (deltaOfRandomness 0.4) (complexity LOW)
    			(interactivity 1.0) (hasCoop TRUE) (pub_name "Holocubierta"))
    (gameGroup (name "Blood Bound") (price 13.0) (EPA 30) (avgTurnLength 6) (deltaOfRandomness 0.2) (complexity MEDIUM)
    			(interactivity 0.7) (hasCoop FALSE) (pub_name "Holocubierta"))
	(gameGroup (name "Cosmic Encounter") (price 50.0) (EPA 80) (avgTurnLength 10) (deltaOfRandomness 0.3) (complexity HIGH)
    			(interactivity 0.3) (hasCoop TRUE) (pub_name "Holocubierta"))
	(gameGroup (name "Dixit") (price 20.0) (EPA 30) (avgTurnLength 7) (deltaOfRandomness 0.4) (complexity LOW)
    			(interactivity 1.0) (hasCoop TRUE) (pub_name "Holocubierta"))
	)

(deftemplate gameParty extends gameBase
	(slot durability (type FLOAT) (default 0))
	(slot flirtiness (type FLOAT) (default 0))
	(slot interactivity (type FLOAT) (default 0))
	(slot betterDrunk (type SYMBOL) (default FALSE) (allowed-values TRUE FALSE)))

(deffacts partyGames
	(gameParty (name "Taboo") (price 20.0) (EPA 20) (avgTurnLength 4) (deltaOfRandomness 0.5) (complexity LOW)
				(durability 0.5) (flirtiness 0.2) (interactivity 0.6) (betterDrunk FALSE)  (pub_name "Holocubierta"))
	(gameParty (name "Twister") (price 20.0) (EPA 10) (avgTurnLength 1) (deltaOfRandomness 0.2) (complexity LOW)
				(durability 0.7) (flirtiness 0.8) (interactivity 1.0) (betterDrunk TRUE) (pub_name "Holocubierta"))
	(gameParty (name "Jungle Speed") (price 13.0) (EPA 10) (avgTurnLength 3) (deltaOfRandomness 0.2) (complexity LOW)
				(durability 0.7) (flirtiness 0.4) (interactivity 0.8) (betterDrunk TRUE) (pub_name "Holocubierta"))
	(gameParty (name "Two Rooms and a Boom") (price 1.0) (EPA 30) (avgTurnLength 3) (deltaOfRandomness 0.1) (complexity MEDIUM)
				(durability 0.9) (flirtiness 0.5) (interactivity 1.0) (betterDrunk FALSE) (pub_name "Holocubierta"))
	(gameParty (name "Say Anything") (price 16.5) (EPA 30) (avgTurnLength 2) (deltaOfRandomness 0.2) (complexity MEDIUM)
				(durability 0.6) (flirtiness 0.6) (interactivity 0.7) (betterDrunk FALSE) (pub_name "Cocktail Games"))
	)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Protègè Instance Generation Rules
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(mapclass :THING)

(defrule createInstances
	(gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) 
		(complexity ?co) (noOfPlayers ?no) (pub_name ?p_name))
	
	(object (is-a PUBLISHER) (OBJECT ?pub) (name ?p_name))
	
	(not (object (is-a GAME) (OBJECT ?c) (name ?n) ))

	=>
	
	(make-instance of GAME (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (publisher ?pub) )
)

(defrule insertStores

	(gameBase (name ?n) (store_names $?stores))

	(object (is-a GAME) (OBJECT ?game) (name ?n) (stores $?g_stores))
	(object (is-a STORE) (OBJECT ?s) (name ?stname))

	(test (not (member$ ?s $?g_stores)))
	;(test (member$ ?stname $?stores))

	=>

	(slot-insert$ ?game stores 1 ?s)
	(slot-insert$ ?s games 1 ?game)

)

(defrule insertDesigners

	(gameBase (name ?n) (des_name ?des_name))

	(object (is-a GAME) (OBJECT ?game) (name ?n) (designer ?designer))
	(object (is-a DESIGNER) (OBJECT ?d) (name ?des_name))

	(test (eq nil (slot-get ?game designer)))

	=>

	(slot-set ?game designer ?d)
	(slot-insert$ ?d games 1 ?game)

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Protègè Instance Classification Rules
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; AGES

(defrule kidsInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  
	
	(gamePEGI3 (name ?n) )
	
    (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_less10")  
           (:DIRECT-INSTANCES $?x))  
    
	(not (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_less10")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 
    
    =>

    ;(printout t ?n)

    ;(printout t (length$ ?x) crlf)

    (slot-set "GAME_AGES_less10" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h)) 
)

(defrule teenagerInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  
	
	(gameBase (name ?n) {price <= 20} {complexity != HIGH} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_10-14")  
           (:DIRECT-INSTANCES $?x))  
    
    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_10-14")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>

    (slot-set "GAME_AGES_10-14" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h)) 
)

(defrule youngAdultInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameBase (name ?n) {price <= 50} {complexity != HIGH} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_15-20")  
        (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_15-20") 
    	(:DIRECT-INSTANCES $? ?me $?)) ) 
    
    =>    

    (slot-set "GAME_AGES_15-20" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h)) 
)

(defrule adultInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameBase (name ?n) {complexity != LOW} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_20up")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_AGES_20up")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 
    =>

    (slot-set "GAME_AGES_20up" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h)) 
)


;; CATEGORY

(defrule newPlayerInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameNewPlayer (name ?n) (hasCoop ?hc))

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_NEW_PLAYER")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_NEW_PLAYER")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>

    ;(slot-set ?me hasCoop ?hc)

    (slot-set "GAME_CATEGORY_NEW_PLAYER" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))

    ;(printout t ?n)
    ;(printout t (length$ ?x) crlf)
)

(defrule soloInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameSolo (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_STRATEGY")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_STRATEGY")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>    

    (slot-set "GAME_CATEGORY_STRATEGY" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule twoPlayerInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameTwoPlayer (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_TWO_PLAYER")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_TWO_PLAYER")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>
    
    (slot-set "GAME_CATEGORY_TWO_PLAYER" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule cheapInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameCheap (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_CHEAP")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_CHEAP")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>
    
    (slot-set "GAME_CATEGORY_CHEAP" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule PEGI3Instances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gamePEGI3 (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_PEGI3")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_PEGI3")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>
    
    (slot-set "GAME_CATEGORY_PEGI3" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule casualInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameCasual (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_COOP")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_COOP")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>
    
    (slot-set "GAME_CATEGORY_COOP" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule groupInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameGroup (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_RPG")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_RPG")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>
    
    (slot-set "GAME_CATEGORY_RPG" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule partyInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	(gameParty (name ?n) )

	(object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_PARTY")  
           (:DIRECT-INSTANCES $?x)) 

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_CATEGORY_PARTY")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>
    
    (slot-set "GAME_CATEGORY_PARTY" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

;; COMPLEXITY

(defrule highCompInstances

	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {complexity == HIGH} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_COMPLEXITY_HIGH")  
           (:DIRECT-INSTANCES $?x))  
    
    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_COMPLEXITY_HIGH")  
        (:DIRECT-INSTANCES $? ?me $?)) )       

    =>

    (slot-set "GAME_COMPLEXITY_HIGH" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))

)

(defrule medCompInstances

	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {complexity == MEDIUM} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_COMPLEXITY_MEDIUM")  
           (:DIRECT-INSTANCES $?x))  
    
    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_COMPLEXITY_MEDIUM")  
        (:DIRECT-INSTANCES $? ?me $?)) )      

    =>

    (slot-set "GAME_COMPLEXITY_MEDIUM" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))

)

(defrule lowCompInstances

	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {complexity == LOW} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_COMPLEXITY_LOW")  
           (:DIRECT-INSTANCES $?x))

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_COMPLEXITY_LOW")  
        (:DIRECT-INSTANCES $? ?me $?)) )   

    =>

    (slot-set "GAME_COMPLEXITY_LOW" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)


;; LENGTH

(defrule veryShortInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA < 15} )
	
    (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_less15")  
           (:DIRECT-INSTANCES $?x))  
    
    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_less15")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>

    (slot-set "GAME_LENGTH_less15" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule shortInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA >= 15 && EPA < 25} )
	
	(object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_15-25")  
           (:DIRECT-INSTANCES $?x))  
    
    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_15-25")  
           (:DIRECT-INSTANCES $? ?me $?)) ) 

    =>

    (slot-set "GAME_LENGTH_15-25" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule mediumLengthInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA >= 25 && EPA < 40} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_25-40")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_25-40")  
           (:DIRECT-INSTANCES $? ?me $?)) )     
    =>

    (slot-set "GAME_LENGTH_25-40" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)


(defrule longInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA >= 40 && EPA < 60} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_40-60")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_40-60")  
           (:DIRECT-INSTANCES $? ?me $?)) )     
    
    =>

    (slot-set "GAME_LENGTH_40-60" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule veryLongInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA >= 60 && EPA < 90} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_60-90")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_60-90")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
    
    =>

    (slot-set "GAME_LENGTH_60-90" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule incrediblyLongInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA >= 90 && EPA < 120} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_90-120")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_90-120")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
    =>

    (slot-set "GAME_LENGTH_90-120" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule eternalInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {EPA >= 120} )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_120up")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_LENGTH_120up")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
    
    =>

    (slot-set "GAME_LENGTH_120up" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

;; PRICE

(defrule veryCheapInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {price < 10}  )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_less10")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_less10")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
    
    =>

    (slot-set "GAME_PRICE_less10" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule CheapInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {price >= 10 && price < 30}  )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_10-30")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_10-30")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
     
    =>

    (slot-set "GAME_PRICE_10-30" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule mediumPricedInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {price >= 30 && price < 50}  )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_30-50")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_30-50")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
     
    =>

    (slot-set "GAME_PRICE_30-50" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule expensiveInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {price >= 50 && price < 75}  )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_50-75")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_50-75")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
     
    =>

    (slot-set "GAME_PRICE_50-75" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)

(defrule veryExpensiveInstances
	?me <- (object (is-a GAME) (OBJECT ?h) (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )  

	( gameBase (name ?n) {price >= 75}  )

    (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_75up")  
           (:DIRECT-INSTANCES $?x))  

    (not (object (is-a :STANDARD-CLASS) (:NAME "GAME_PRICE_75up")  
           (:DIRECT-INSTANCES $? ?me $?)) )  
     
    =>

    (slot-set "GAME_PRICE_75up" :DIRECT-INSTANCES  
       (insert$ ?x (+ 1 (length$ ?x)) ?h))
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auxiliary Functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deffunction maxTurnLength (?likesToWait ?timeToPlay ?EPA ?avgTurnLength)
	(return (* (/ ?timeToPlay (/ ?EPA ?avgTurnLength)) (/ ?likesToWait 2)))
	)

(deffunction minDurability (?durable ?budget ?age)
    (if (eq ?durable TRUE) then
        (return (+ (+ (* (/ 50 ?budget) 0.25) (* (/ 5 ?age) 0.25)) 0.25))
    else
        (return (+ (* (/ 50 ?budget) 0.25) (* (/ 5 ?age) 0.25)))
    )
)

(deffunction maxDelta (?likesSurprises ?likesThink)
    (if (eq ?likesSurprises TRUE) then
        (if (eq ?likesThink TRUE) then
            (return 0.75)
        else
            (return 1)
        )
    else
        (return 0.5)
    )
)

(deffunction maxComplexity (?likesThink ?age ?mentalEnergy)
    (if (eq ?likesThink TRUE) then
        (return (+ 0.25 (+ (/ ?age 15) ?mentalEnergy)) )
    else
        (return (- 0.25 (+ (/ ?age 15) ?mentalEnergy)) )
    )
)

(deffunction minReplayability (?budget)
    (return (/ 10 ?budget) )
)

(deffunction canBuyExpansions (?price ?budget)
    (if (>= (- ?budget ?price) (/ ?price 2)) then
        (return TRUE)
    else
        (return FALSE)
    )
)

(deffunction needsToBeReusable (?bu ?age)
    (if (<= ?age 15) then
        (return TRUE)
    else
        (if (<= ?bu 10) then
           (return TRUE)
        else
           (return FALSE)
        )
    )
)

(deffunction minInteractivity (?nplayers ?maxTurnLength)
    (return (+ (* 0.5 (/ ?nplayers 10)) (* 0.5 (/ ?maxTurnLength 10))))
)

(deffunction getAgeType (?age)
	(if (<= ?age 10) then
		(return "GAME_AGE_less10")
	else 
		(if (and (>= ?age 10) (< ?age 14)) then
			(return "GAME_AGE_10-14")
		else
			(if (and (>= ?age 14) (< ?age 20)) then
				(return "GAME_AGE_14-20")
			else 
				(return GAME_AGE_20up)
			)
		)
	)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Recommendations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule recommend

	(object (is-a MYUSER) (OBJECT ?user) (name ?u_name) (recommendations $?rec) (numFriends ?nf)
		(isCompetitive ?iscomp) (likesToWait ?lw) (timeToPlay ?tp) (likesSurprises ?ls)
        (wantsToThink ?think) (budget ?bu) (age ?age) (mentalEnergy ?me) (schwifty ?sw) )

    (object (is-a GAME) (OBJECT ?game) (name ?g_name) (price ?p)(EPA ?epa) (avgTurnLength ?avt) (deltaOfRandomness ?delt)
    								  (complexity ?co) (noOfPlayers ?no) (:DIRECT-TYPE $?types) )  
    
    (test (not (member$ ?g_name $?rec)))
    
    ; Generic
    (test (<= ?epa ?tp))
    (test (<= ?p ?bu))
    (test (<= ?avt (maxTurnLength ?lw ?tp ?epa ?avt)))
    (test (>= ?nf (- ?no 1)))
    (test (<= ?delt (maxDelta ?ls ?think)))
    ;(test (member$ (getAgeType ?age) $?types))

	=>

	(printout t ?u_name ": " ?g_name crlf)
	(slot-insert$ ?user recommendations 1 ?g_name)
)

(reset)
(run)
;(focus MAIN)
