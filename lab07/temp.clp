

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; CATEGORY

(defrule newPlayerInstances
	(gameNewPlayer (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
				(hasCoop ?hc) )
	=>
	(make-instance of GAME_CATEGORY_NEW_PLAYER (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) 
	(hasCoop ?hc) )
)


(defrule soloInstances
	(gameSolo (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
				(theme ?th) (immersion ?imm) (replayability ?rp) (hasExpansions ?he) )
	=>
	(make-instance of GAME_CATEGORY_SOLO (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
	(theme ?th) (immersion ?imm) (replayability ?rp) (hasExpansions ?he) )
)

(defrule twoPlayerInstances
	(gameTwoPlayer (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
				(hasCoop ?hc) (hasExpansions ?he) (replayability ?rp) (rubberbanding ?rb) )
	=>
	(make-instance of GAME_CATEGORY_TWO_PLAYER (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
	(hasCoop ?hc) (hasExpansions ?he) (replayability ?rp) (rubberbanding ?rb) )
)

(defrule cheapInstances
	(gameCheap (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
				(hasCoop ?hc) (replayability ?rp) (areComponentsReusable ?reus) (hasCards ?hc) (hasBoard ?hb) (hasDice ?hd) )
	=>
	(make-instance of GAME_CATEGORY_CHEAP (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)
	(hasCoop ?hc) (replayability ?rp) (areComponentsReusable ?reus) (hasCards ?hc) (hasBoard ?hb) (hasDice ?hd)  )
)

(defrule PEGI3Instances
	(gamePEGI3 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) 
				(theme ?th) (immersion ?imm) (funForParents ?ffp) (durability ?dur) )
	=>
	(make-instance of GAME_CATEGORY_PEGI3 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (theme ?th) (immersion ?imm) (funForParents ?ffp) (durability ?dur) )
)

(defrule casualInstances
	(gameCasual (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no)  (isPortable ?p) (setupTime ?st) (replayability ?rp) )
	=>
	(make-instance of GAME_CATEGORY_CASUAL (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (isPortable ?p) (setupTime ?st) (replayability ?rp) )
)

(defrule groupInstances
	(gameGroup (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (interactivity ?int)(hasCoop ?coop) )
	=>
	(make-instance of GAME_CATEGORY_GROUP (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (interactivity ?int) (hasCoop ?coop) )
)

(defrule partyInstances
	(gameParty (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (durability ?dur) (flirtiness ?fl) (interactivity ?int) (betterDrunk ?dru))
	=>
	(make-instance of GAME_CATEGORY_PARTY (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?co) (noOfPlayers ?no) (durability ?dur) (flirtiness ?fl) (interactivity ?int) (betterDrunk ?dru))
)

;; COMPLEXITY

(defrule highCompInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) {complexity == HIGH} (noOfPlayers ?no) )
	=> 
	( make-instance of GAME_COMPLEXITY_HIGH (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (noOfPlayers ?no) )
)

(defrule medCompInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) {complexity == MEDIUM} (noOfPlayers ?no) )
	=> 
	( make-instance of GAME_COMPLEXITY_MEDIUM (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (noOfPlayers ?no) )
)

(defrule lowCompInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) {complexity == LOW} (noOfPlayers ?no) )
	=> 
	( make-instance of GAME_COMPLEXITY_LOW (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (noOfPlayers ?no) )
)

;; LENGTH

(defrule veryShortInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (< ?epa 15) )
	=> 
	( make-instance of GAME_LENGTH_less15 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

(defrule shortInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?epa 15) )
	(test (< ?epa 25) )
	=> 
	( make-instance of GAME_LENGTH_15-25 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

(defrule mediumLengthInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?epa 25) )
	(test (< ?epa 40) )
	=> 
	( make-instance of GAME_LENGTH_25-40 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

(defrule longInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?epa 40) )
	(test (< ?epa 60) )
	=> 
	( make-instance of GAME_LENGTH_40-60 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

(defrule veryLongInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?epa 60) )
	(test (< ?epa 90) )
	=> 
	( make-instance of GAME_LENGTH_60-90 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

(defrule incrediblyLongInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?epa 90) )
	(test (< ?epa 120) )
	=> 
	( make-instance of GAME_LENGTH_90-120 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

(defrule eternalInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?epa 120) )
	=> 
	( make-instance of GAME_LENGTH_120up (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

;; PRICE

( defrule veryCheapInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (< ?p 10) )
	=>
	( make-instance of GAME_PRICE_less10 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

( defrule cheapInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?p 10) )
	(test (< ?p 30) )
	=>
	( make-instance of GAME_PRICE_10-30 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

( defrule mediumPricedInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?p 30) )
	(test (< ?p 50) )
	=>
	( make-instance of GAME_PRICE_30-50 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

( defrule expensiveInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?p 50) )
	(test (< ?p 75) )
	=>
	( make-instance of GAME_PRICE_50-75 (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)

( defrule veryExpensiveInstances
	( gameBase (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
	(test (>= ?p 75) )
	=>
	( make-instance of GAME_PRICE_75up (name ?n) (price ?p) (EPA ?epa) (avgTurnLength ?avg) (deltaOfRandomness ?delt) (complexity ?c) (noOfPlayers ?no) )
)
