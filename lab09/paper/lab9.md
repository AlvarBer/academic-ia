% Artificial Inteligence - Lab 9
% Álvaro Bermejo ; Borja Lorente
% April 2016

Supervised Learning
===================

J48
---
As expected training set got the highest accuracy (97 %) and a better result on 
other metrics such as ROC or F-measure, while test set and ten-fold only 
differed in only 2.5 % (90 to 92 respectively), this difference can be 
attributed to the fact that we test a smaller population when we are doing the 
ten-fold.

This hyptothesis can be further explained by the fact that the tree produced by 
all runs is the same, having twelve leaves and a size of twenty-one and the same 
exact leaves. 

ZeroR
-----
Test set and training set gives us here the same exact results, a merely 45.5 % 
accuracy on predicition, and when we check the confusion matrix we can see that 
the classifier is merely always otputing class e (Drug Y).

The stratified ten-fold gives us the same result, but with a slightly smaller 
ROC area, still the classifier keeps doing the previous behaviour.

Unsupervised Learning
=====================

In order to check the accuracy of the clustering the only convenient way we have 
is applying class based clustering, we select hierarchical clustering with five 
clusters, which gives us an accuracy of 39 %, we could boost this percentage up 
to 44 % by only telling the algorithm to look for two or three clusters.

With these results and checking the Classed to clusters table we can infer that 
examples of drug A, B, C get compressed into a single cluster, the reason for 
it probably being the low number of examples of those compared to examples of 
drug X or Y, one way we could solve this could be through generation of 
synthetic data with methods like Smote.

After feature engineering
=========================

Supervised Learning
-------------------
J48 gets a 100 % accuracy on the training set and a 99 % accuracy on the 
ten-fold,however only a 57.5 % on the test set, these result may appear to be 
caused by an overfitting on the stadistical model.

ZeroR doesn't change the result in a stadistical significant way.

Unsupervised Learning
---------------------
Hierarchical clusterings gets about the same results, even in some cases 
slighty worse, which would give more credibility to the theory of the 
overfitting caused by our feature engineering

Graphics
========
![Samples class discrepancy](images/DrugClasses.png)

In this graphic we see the imbalance of samples with labels Drug X or Drug Y 
compared to the samples with other labels.

![K/NA relation](images/K-Na.png)

We coloured here drug Y as black and the rest of the other drugs as other 
colours, and we can clearly stablish a correlation between this relation and 
the drug, unfortunately the other drugs don't have such a clear division.

![J48 Tree](images/Tree.png)

This is a drawing of part of the decision tree generated by the J48 alogithm.s

Conclusions
===========
The experiments we did show us how feature engineering can rise an accuracy 
gain, and at the same time cause overfitting, which tells us the importance of 
always splitting the train and test sets.

The big imbalance between the number of examples labeled as one class is a very 
big burden in order to make accurate predictions, specially when the samples 
belong to those under-represented labels.
