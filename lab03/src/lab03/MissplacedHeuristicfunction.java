package lab03;

import aima.core.search.framework.HeuristicFunction;

/**
 * Created by mortadelegle on 29/11/15.
 */
public class MissplacedHeuristicfunction implements HeuristicFunction{
	@Override
	public double h(Object o) {
		int i, blacksFound = 0, whitesFound = 0;

		FrogPuzzleBoard board = (FrogPuzzleBoard) o;

		i = 0;
		while(board.state[i] != Frog.WHITE) {
			if(board.state[i] == Frog.BLACK)
				++blacksFound;
			++i;
		}

		i = board.state.length - 1;
		while(board.state[i] != Frog.BLACK) {
			if(board.state[i] == Frog.WHITE)
				++whitesFound;
			--i;
		}

		return board.state.length - blacksFound - whitesFound;

	}
}
