package lab03;

import aima.core.agent.Action;
import aima.core.search.framework.*;
import aima.core.search.informed.AStarSearch;
import aima.core.search.informed.GreedyBestFirstSearch;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;


/**
 * Created by mortadelegle on 26/11/15.
 */
public class DemoInformedSearches {
	private static FrogPuzzleBoard initialBoard = new FrogPuzzleBoard();
	private static FPStepCostFunction stepCost = new FPStepCostFunction();

	public static void main (String[] args) {
		frogDemo();
	}

	private static void frogDemo() {
		System.out.println("Doing Informed Searches");

		Problem problem = new Problem(initialBoard, FrogPuzzleFunctionFactory.getActionsFunction(),
				FrogPuzzleFunctionFactory.getResultFunction(), new FrogPuzzleGoalTest(), stepCost);

		Search aStarMisSearch = new AStarSearch(new GraphSearch(), new MissplacedHeuristicfunction());
		Search aStarFinSearch = new AStarSearch(new GraphSearch(), new FinalDistanceHeuristicFunction());
		Search greedyMisSearch = new GreedyBestFirstSearch(new GraphSearch(), new MissplacedHeuristicfunction());
		Search greedyFinSearch = new GreedyBestFirstSearch(new GraphSearch(), new FinalDistanceHeuristicFunction());
		Search greedyMisFirstSearch = new GreedyBestFirstSearch(new GraphSearch(), new MissplacedHeuristicfunction());
		Search greedyFinFirstSearch = new GreedyBestFirstSearch(new GraphSearch(), new FinalDistanceHeuristicFunction());

		try {
			System.out.println("A* with Misplaced Heuristic --->>>>>");
			SearchAgent agent = new SearchAgent(problem, aStarMisSearch);
			printInstrumentation(agent.getInstrumentation());
			System.out.println("A* with Final Distances Heuristic --->>>>>");
			agent = new SearchAgent(problem, aStarFinSearch);
			printInstrumentation(agent.getInstrumentation());
			System.out.println("Greedy with Missplaced Heuristic --->>>>>");
			agent = new SearchAgent(problem, greedyMisSearch);
			printInstrumentation(agent.getInstrumentation());
			System.out.println("Greedy with Final Distances Heuristic --->>>>>");
			agent = new SearchAgent(problem, greedyFinSearch);
			printInstrumentation(agent.getInstrumentation());
			System.out.println("Greedy Best First Misplaced Heuristic --->>>>>");
			agent = new SearchAgent(problem, greedyMisFirstSearch);
			printInstrumentation(agent.getInstrumentation());
			System.out.println("Greedy Best First Final Distances Heuristic --->>>>>");
			agent = new SearchAgent(problem, greedyFinFirstSearch);
			printInstrumentation(agent.getInstrumentation());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private static void printActions(List<Action> actions) {
		System.out.println("Printing Actions: ");
		for (Action action : actions) {
			System.out.println(action);
		}
	}

	private static void printInstrumentation(Properties properties) {
		System.out.println("Printing Instrumentation: ");
		Iterator<Object> keys = properties.keySet().iterator();
		while(keys.hasNext()) {
			String key = (String) keys.next();
			String property = properties.getProperty(key);
			System.out.println(key + " : " + property);
		}
	}
}
