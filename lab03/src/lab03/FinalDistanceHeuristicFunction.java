package lab03;

import aima.core.search.framework.HeuristicFunction;

/**
 * Created by Kerith on 26/11/2015.
 */
public class FinalDistanceHeuristicFunction implements HeuristicFunction {

	@Override
	public double h(Object o) {
		FrogPuzzleBoard board = (FrogPuzzleBoard) o;

		int heuristic = 0;
		int blacksFound = 0;
		int whitesFound = 0;

		//Forward
		for (int i = 0; i < 7; i++) {
			if (board.state[i] == Frog.WHITE) {
				if (i != 0)
					heuristic += (i - blacksFound - 1);
				blacksFound++;
			}
		}

		//Backward
		for (int i = 6; i >= 0; i--) {
			if (board.state[i] == Frog.BLACK) {
				if (i != 6)
					heuristic += ((6 - i) - whitesFound - 1);
				whitesFound++;
			}
		}

		return heuristic / 2;

	}
}
