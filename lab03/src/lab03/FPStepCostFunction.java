package lab03;

import aima.core.agent.Action;
import aima.core.search.framework.StepCostFunction;

/**
 * Created by mortadelegle on 2/11/15.
 */
public class FPStepCostFunction implements StepCostFunction {
    @Override
    public double c(Object o, Action action, Object o1) {
        if((action.equals(FrogPuzzleBoard.MOVEHOLE3RIGHT)) || action.equals(FrogPuzzleBoard.MOVEHOLE3LEFT))
            return 2;
        else
            return 1;
    }
}
