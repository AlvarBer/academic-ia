package lab03;

import aima.core.agent.Action;
import aima.core.agent.impl.DynamicAction;

import java.util.Arrays;

/**
 * Created by mortadelegle on 2/11/15.
 */
public class FrogPuzzleBoard {
    Frog state[]; // 0 -> empty, 1 -> black, 2 -> white

    public static Action MOVEHOLE1LEFT = new DynamicAction("1Left");
    public static Action MOVEHOLE2LEFT = new DynamicAction("2Left");
    public static Action MOVEHOLE3LEFT = new DynamicAction("3Left");
    public static Action MOVEHOLE1RIGHT = new DynamicAction("1Right");
    public static Action MOVEHOLE2RIGHT = new DynamicAction("2Right");
    public static Action MOVEHOLE3RIGHT = new DynamicAction("3Right");

    // Constructor
    public FrogPuzzleBoard() {
        state = new Frog[]{Frog.BLACK, Frog.BLACK, Frog.BLACK, Frog.SPACE,
                Frog.WHITE, Frog.WHITE, Frog.WHITE};
    }

    public FrogPuzzleBoard(FrogPuzzleBoard board) {
        this.state = new Frog[7];
        System.arraycopy(board.state, 0, this.state, 0, this.state.length);
    }

    // Operators
    public boolean canMoveHole(Action where) {
        if(where.equals(MOVEHOLE1LEFT)) {
            return whereIsHole() > 0;
        }
        else if(where.equals((MOVEHOLE2LEFT))) {
            return whereIsHole() > 1;
        }
        else if(where.equals((MOVEHOLE3LEFT))) {
            return whereIsHole() > 2;
        }
        else if(where.equals(MOVEHOLE1RIGHT)) {
            return whereIsHole() < 6;
        }
        else if(where.equals((MOVEHOLE2RIGHT))) {
            return whereIsHole() < 5;
        }
        else if(where.equals((MOVEHOLE3RIGHT))) {
            return whereIsHole() < 4;
        }
        else {
            try {
                throw new Exception("No more actions could be applied");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    public void move(Action action) {
        if(action.equals(MOVEHOLE1LEFT)) {
            moveHole1L();
        }
        else if(action.equals((MOVEHOLE2LEFT))) {
            moveHole2L();
        }
        else if(action.equals((MOVEHOLE3LEFT))) {
            moveHole3L();
        }
        else if(action.equals(MOVEHOLE1RIGHT)) {
            moveHole1R();
        }
        else if(action.equals((MOVEHOLE2RIGHT))) {
            moveHole2R();
        }
        else if(action.equals((MOVEHOLE3RIGHT))) {
            moveHole3R();
        }
        else {
            try {
                throw new Exception("We are so fucked");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void moveHole1L() {
        int hole = whereIsHole();
        swap(hole, hole - 1);
    }
    public void moveHole2L() {
        int hole = whereIsHole();
        swap(hole, hole - 2);
    }
    public void moveHole3L() {
        int hole = whereIsHole();
        swap(hole, hole - 3);
    }
    public void moveHole1R() {
        int hole = whereIsHole();
        swap(hole, hole + 1);
    }
    public void moveHole2R() {
        int hole = whereIsHole();
        swap(hole, hole + 2);
    }
    public void moveHole3R() {
        int hole = whereIsHole();
        swap(hole, hole + 3);
    }

    private void swap(int from, int to) {
        Frog tmp = state[from];
        state[from] = state[to];
        state[to] = tmp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FrogPuzzleBoard that = (FrogPuzzleBoard) o;

        return Arrays.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return state != null ? Arrays.hashCode(state) : 0;
    }

    private int whereIsHole() {
        int i = 0;
        while(state[i] != Frog.SPACE) {
            ++i;
        }

        return i;
    }
}
