package lab03;

import aima.core.search.framework.GoalTest;

public class FrogPuzzleGoalTest implements GoalTest {
    @Override
    public boolean isGoalState(Object o) {
        int i = 0, whiteFrogs = 0;

        FrogPuzzleBoard board = (FrogPuzzleBoard) o;
        while(i < board.state.length && board.state[i] != Frog.BLACK) {
            if(board.state[i++] == Frog.WHITE)
                ++whiteFrogs;
        }
        
        return (whiteFrogs == 3);
    }
}
