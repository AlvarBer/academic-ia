package lab03;

import aima.core.agent.Action;
import aima.core.search.framework.*;
import aima.core.search.uninformed.*;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class DemoUninformedSearches {
    private static FrogPuzzleBoard initialBoard = new FrogPuzzleBoard();
    private static FPStepCostFunction stepCost = new FPStepCostFunction();

    public static void main (String[] args) {
        frogDemo();
    }

    private static void frogDemo() {
        System.out.println("Doing Uninformed searches");

        Problem problem = new Problem(initialBoard, FrogPuzzleFunctionFactory.getActionsFunction(),
                FrogPuzzleFunctionFactory.getResultFunction(), new FrogPuzzleGoalTest(), stepCost);
	    Search searchDepthFirst = new DepthFirstSearch(new GraphSearch());
        Search searchDepthLimited = new DepthLimitedSearch(50);
        Search searchBreadthFirst = new BreadthFirstSearch();
        Search searchIterativeDepth = new IterativeDeepeningSearch();
	    Search searchUniformCost = new UniformCostSearch();
        try {
	        System.out.println("Depth First --->>>>>");
	        SearchAgent agent = new SearchAgent(problem, searchDepthFirst);
	        printInstrumentation(agent.getInstrumentation());
	        System.out.println("Depth Limited --->>>>>");
            agent = new SearchAgent(problem, searchDepthLimited);
            printInstrumentation(agent.getInstrumentation());
            System.out.println("Breadth First --->>>>>");
            agent = new SearchAgent(problem, searchBreadthFirst);
            printInstrumentation(agent.getInstrumentation());
            System.out.println("Iterative Deepening Search --->>>>>");
            agent = new SearchAgent(problem, searchIterativeDepth);
            printInstrumentation(agent.getInstrumentation());
	        System.out.println("Uniform Cost --->>>>>");
	        agent = new SearchAgent(problem, searchUniformCost);
	        printInstrumentation(agent.getInstrumentation());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void printActions(List<Action> actions) {
        System.out.println("Printing Actions: ");
	    for (Action action : actions) {
		    System.out.println(action);
	    }
    }

    private static void printInstrumentation(Properties properties) {
		System.out.println("Printing Instrumentation: ");
        Iterator<Object> keys = properties.keySet().iterator();
        while(keys.hasNext()) {
            String key = (String) keys.next();
            String property = properties.getProperty(key);
            System.out.println(key + " : " + property);
        }
    }
}
