package lab03;

import aima.core.agent.Action;
import aima.core.search.framework.ActionsFunction;
import aima.core.search.framework.ResultFunction;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by mortadelegle on 2/11/15.
 */
public class FrogPuzzleFunctionFactory {

    private static ActionsFunction _actionsFunction = null;
    private static ResultFunction _resultFunction = null;

    public static ActionsFunction getActionsFunction() {
        if(_actionsFunction == null) {
            _actionsFunction = new FPActionsFunction();
        }
        return _actionsFunction;
    }

    public static ResultFunction getResultFunction() {
        if(_resultFunction == null) {
            _resultFunction = new FPResultfunction();
        }
        return _resultFunction;
    }

    private static class FPResultfunction implements ResultFunction {
        @Override
        public Object result(Object o, Action action) {
            FrogPuzzleBoard board = (FrogPuzzleBoard) o;
            FrogPuzzleBoard newBoard = new FrogPuzzleBoard(board);
            if(board.canMoveHole(action)) {
	            newBoard.move(action);
            }

            return newBoard;
        }
    }

    private static class FPActionsFunction implements ActionsFunction {
        @Override
        public Set<Action> actions(Object o) {
            FrogPuzzleBoard board = (FrogPuzzleBoard) o;
            Set<Action> set = new LinkedHashSet<>();

            Action actionArray[] = new Action[] { FrogPuzzleBoard.MOVEHOLE1LEFT, FrogPuzzleBoard.MOVEHOLE2LEFT,
                    FrogPuzzleBoard.MOVEHOLE3LEFT, FrogPuzzleBoard.MOVEHOLE1RIGHT, FrogPuzzleBoard.MOVEHOLE2RIGHT,
                    FrogPuzzleBoard.MOVEHOLE3RIGHT};

            for(int i = 0; i < actionArray.length; ++i) {
                if (board.canMoveHole(actionArray[i])) {
                    set.add(actionArray[i]);
                }
            }

            return set;
        }
    }
}
